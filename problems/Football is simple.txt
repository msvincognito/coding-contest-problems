Football is simple. You kick the ball and hope for the best (at least that is how the author of the question plays the game).

This problem is similar to the football. It is simple.

Lately, guessing outcome of football games became popular at DKE. So, why not combine programming skills for better prediction of games, or in this case number of goals scored by the team.

It is known that, some betting options have higher coefficients. For example, one interesting task might be to find out the probability that one of the teams will end up with prime number of goals scored by the end of the game.

Well, you can just randomly guess this. We will give you three numbers to solve this problem. It is known that each team has some chance P(i) of scoring in given intervals. This probability will be the same for each interval.
Also, it is known that each team can score at most one goal per interval. So, in each line of test case you will get three numbers : number of intervals and two numbers representing chances of scoring in each interval for each team.

One example to explain it:
Input line might be like this - 18 50 50
This will mean there are 18 intervals in the game that we will consider and for each interval there is 50% chance of scoring a goal for both team 1 and team 2.
Answer to this test case is 0.5266. I hope questions is clear now.

So, go ahead, work your magic and win shitloads of money. Oh, also, do not worry, amount of intervals will be quite small (at max : 18).

Output the answer with 4 digits after decimal.