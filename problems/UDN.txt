Unique Digit Numbers are numbers composed of maximally 9 unique digits. Examples are 16542, 12375, but not 122 or 1234567890. 

A UDN can be factored into multiple UDNs, for example, 20 can be factored into 1, 2 and 10. A maximal factorization is when a UDN is factored into as many as possible UDNs. For example, the maximal factorization of 12375 is 1, 5, 15 and 165. 

Your task is to find a UDN with an as large as possible maximal factorization. 

When submitting your answer, send the UDN, its factorization and any code you wrote to get your answer. 

The submission with the largest factorization will receive 10 points, the second largest 9 points, etc. In case of a tie, both submissions will receive the same number of points. 