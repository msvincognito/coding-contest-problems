Programming languages are not always meant to be fast, readable or extensible, sometimes they don't even have practical application. Most of the esoteric programming languages (http://en.wikipedia.org/wiki/Esoteric_programming_language) don't.

Brainfuck is an esoteric programming language, with just a few commands. The lanuage can be learned from its Wikipedia page (http://en.wikipedia.org/wiki/Brainfuck). 

The goal of this problem is quite simple: write a program that takes an arbitrary string as input and returns the shortest possible brainfuck code printing that string. 

Sample input:

Hello World!

Sample output:

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.+++++++++++++++++++++++++++++.+++++++..+++.-------------------------------------------------------------------------------.+++++++++++++++++++++++++++++++++++++++++++++++++++++++.++++++++++++++++++++++++.+++.------.--------.-------------------------------------------------------------------.

It might be obvious that this is not the shortest possible output, that's where the scoring system for this question comes in. Your program will be tested with 10 pre-defined, secret input strings. The submission producing the least characters of brainfuck code in total for all input strings gets 10 points, the second 9, and so on. In case of a tie in length, the winner will be decided by running time.

Additional requirements are that your program should finish within 5 minutes. If it does not finish within the time limit, crashes, or produces invalid output a 1000 character penalty will be added to your total output length.