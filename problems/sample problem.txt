Problem:

Add the numbers given on stdin and print the sum to stdout. 

Input:

A set of numbers through stdin, one on each line, ended by -1 (do not add this -1 to the sum). 

Output:

One line with the sum of the given numbers to stdout. 
