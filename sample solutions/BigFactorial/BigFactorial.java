package solutionCode;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class BigFactorial {
	
	private int sizeOfArray = 100000;
	
	// Solution to the big factorial question
	// Limit :
	// Small dataset : 1 <= n <= 30
	// Large dataset : 60 <= n <= 400
	// These limitations can change.
	
	public void printSolution(long n)
	{
		byte[] solution = new byte[sizeOfArray];
		solution[sizeOfArray-1] = 1;
		int j = 1;
		for (int i=2;i<=n;i++)
		{
			
			long divisor = 0;
			long remainder = 0;
			for (int t = sizeOfArray - 1; t>=sizeOfArray - j;t--)
			{
				long k = i * solution[t] + divisor;
				remainder = k % 10;
				divisor = k / 10;
				solution[t] = (byte) remainder;
			}
			while (divisor > 0)
			{
				remainder = divisor % 10;
				divisor = divisor / 10;
				j = j+1;
				solution[sizeOfArray - j] = (byte) remainder;
			}
		}
		for (int i = sizeOfArray - j;i<sizeOfArray;i++)
			System.out.print(solution[i]);
		
		System.out.println();

	}
	
	public static void main(String[] args) throws FileNotFoundException
	{		
		FileInputStream file = new FileInputStream("C:\\Users\\Taghi\\eclipseWorkspace\\CodingContest\\inputs\\bigSetFactorial.txt");
		DataInputStream in2 = new DataInputStream(file);
		Scanner in = new Scanner(new InputStreamReader(in2));
		int numberOfTestCases = in.nextInt();
		BigFactorial engine = new BigFactorial();
		for (int i=0;i<numberOfTestCases;i++)
		{
			int n = in.nextInt();
			engine.printSolution(n);
		}
	}

}
