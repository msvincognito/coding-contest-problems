package solutionCode;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Scanner;

public class Bobby {

	public static void main(String[] args) throws FileNotFoundException {

		FileInputStream fileText = new FileInputStream(
				"C:\\Users\\Taghi\\eclipseWorkspace\\coding-contest-problems\\problem input\\Bobby the poor librarian\\text.txt");
		DataInputStream in2Text = new DataInputStream(fileText);
		Scanner inText = new Scanner(new InputStreamReader(in2Text));

		String words = inText.nextLine();
		String[] searchStrings = words.split("\\|");

		HashMap<Integer, String[]> mapping = new HashMap<Integer, String[]>();
		for (int i = 0; i < searchStrings.length - 1; i++) {
			mapping.put(i, searchStrings[i].split(" "));
		}

		int[] counter = new int[searchStrings.length - 1];
		int[] occurences = new int[searchStrings.length - 1];
		for (int i = 0; i < counter.length; i++) {
			counter[i] = 0;
			occurences[i] = 0;
		}

		String text = searchStrings[searchStrings.length - 1];
		Scanner reader = new Scanner(text);
		reader.useDelimiter(" ");
		while (reader.hasNext()) {
			String nextPart = reader.next().toLowerCase();
			for (int j = 0; j < counter.length; j++) {
				if (nextPart.contains(mapping.get(j)[counter[j]].toLowerCase())) {
					counter[j]++;
					if (counter[j] == mapping.get(j).length) {
						counter[j] = 0;
						occurences[j]++;
					}
				} else {
					if (nextPart.contains(mapping.get(j)[0].toLowerCase()))
						counter[j] = 1; // in case like bobby bobby is a good librarian. End might also mean, thing started from first word again.
					else
						counter[j] = 0;
				}
			}
		}

		for (int i = 0; i < counter.length; i++)
			System.out.println(occurences[i]);

	}

}
