import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Scanner;

public class BobbyLars {

	public static void main(String[] args) throws FileNotFoundException {

		FileInputStream fileText = new FileInputStream(
				"problem input\\Bobby the poor librarian\\test_input.txt");
		DataInputStream in2Text = new DataInputStream(fileText);
		Scanner inText = new Scanner(new InputStreamReader(in2Text));

		while (inText.hasNextLine()) {
			String words = inText.nextLine();
			String[] searchStrings = words.split("\\|");

			int[] counter = new int[searchStrings.length - 1];
			int[] occurences = new int[searchStrings.length - 1];

			String text = searchStrings[searchStrings.length - 1];
			for (int i = 0, n = text.length(); i < n; i++) {
				char c = text.charAt(i);
				for (int j = 0; j < searchStrings.length - 1; j++) {
					if (c == searchStrings[j].charAt(counter[j])) {
						counter[j]++;
						if (counter[j] == searchStrings[j].length()) {
							occurences[j]++;
							counter[j] = 0;
						}
					} else {
						counter[j] = 0;
					}
				}
			}

			for (int i = 0; i < counter.length; i++) {
				System.out.println(occurences[i]);
			}
			System.out.println();
		}

	}

}
