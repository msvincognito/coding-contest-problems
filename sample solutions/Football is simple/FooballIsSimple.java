package solutionCode;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class FooballIsSimple {

	public static double findProbability(int intervals, int n1, int n2) {

		double probability = 0.0;

		double p1 = n1 / 100.0;
		double p2 = n2 / 100.0;

		double sum1 = 0.0;
		double sum2 = 0.0;

		ArrayList<Integer> possibilities = possibilities(intervals);
		ArrayList<Double> cases = cases(intervals, possibilities);
		for (int i = 0; i < possibilities.size(); i++) {
			sum1 += Math.pow(p1, possibilities.get(i))
					* Math.pow(1 - p1, intervals - possibilities.get(i)) * cases.get(i);
			sum2 += Math.pow(p2, possibilities.get(i))
					* Math.pow(1 - p2, intervals - possibilities.get(i)) * cases.get(i);
		}

		probability = (sum1 + sum2) - sum1 * sum2;

		return probability;
	}
	
	public static ArrayList<Double> cases(int interval, ArrayList<Integer> possibilities)
	{
		ArrayList<Double> cases = new ArrayList<Double>();
		double nF = factorial(interval);
		for (int i=0;i<possibilities.size();i++)
		{
			double nRf = factorial (interval - possibilities.get(i));
			double rF = factorial(possibilities.get(i));
			cases.add(nF/(nRf * rF));
		}
		
		return cases;
	}
	
	public static ArrayList<Integer> possibilities(int intervals)
	{
		ArrayList<Integer> answer = new ArrayList<Integer>();
		int[] possibilities = {2,3,5,7,11,13,17};
		for (int i = 0;i<possibilities.length;i++)
		{
			if (possibilities[i] <= intervals)
				answer.add(possibilities[i]);
			else
				break;
		}
		return answer;
	}
	
	public static double factorial(int number)
	{
		double result = 1.0;
		for (long i = 2; i <= number; i++)
		{
			result *= i;
		}
		
		return result;
	}

	public static void main(String[] args) throws IOException {
		
		FileInputStream file = new FileInputStream("C:\\Users\\Taghi\\eclipseWorkspace\\CodingContest\\inputs\\input.txt");
		DataInputStream in2 = new DataInputStream(file);
		Scanner in = new Scanner(new InputStreamReader(in2));
		ArrayList<Double> answers = new ArrayList<Double>();
		
		in.useLocale(new Locale("US"));
		int numberOfTestCases = in.nextInt();
		for (int i = 0; i < numberOfTestCases; i++) {
			int intervals = in.nextInt();
			int n1 = in.nextInt();
			int n2 = in.nextInt();
			answers.add(findProbability(intervals, n1, n2));
		}

		for (double tmp : answers)
			System.out.printf(Locale.US, "%.4f\n", tmp);

	}

}
