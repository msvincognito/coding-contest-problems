import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.util.Random;

import javax.imageio.ImageIO;

/**
 * 
 * Generates the masking the secret problems.
 * 
 * Explanation:
 * 1. Takes a message and qr-code as input, where the message
 * length is exactly the number of black pixels in the qr-code.
 * 
 * 2. It maps the
 * characters of the message to these black pixels and adds random characters
 * (a-zA-Z) to the white pixels.
 * 
 * 3. Then it applies a Ceasar/shift cipher (shift
 * all characters by x places in the alphabet, in this case -3) to these
 * characters.
 * 
 * 4. Finally, the encoded message is just the characters read from
 * start to end of the pixels in the image.
 * 
 * @author Lars Mennen <mennen.lars@gmail.com>
 * 
 */
public class MaskingTheSecretGenerator {

	public static void main(String[] args) throws Exception {

		// Read in the small qr-code pixel array
		BufferedImage bufferedImage = ImageIO.read(new File(
				"sample solutions/Masking the secret/qrcode_small.png"));
		byte[] pixels = ((DataBufferByte) bufferedImage.getRaster()
				.getDataBuffer()).getData();

		// True is black, false is white
		boolean[] mask = new boolean[pixels.length / 4];
		int messageLength = 0;

		for (int i = 0; i < pixels.length; i += 4) {
			if (pixels[i] == -1 && pixels[i + 1] == -1 && pixels[i + 2] == -1
					&& pixels[i + 3] == -1) {
				mask[i / 4] = false;
			} else if (pixels[i] == -1 && pixels[i + 1] == 0
					&& pixels[i + 2] == 0 && pixels[i + 3] == 0) {
				mask[i / 4] = true;
				messageLength++;
			} else {
				throw new Exception("Input file is not black and white only!");
			}
		}

		String inputText = "Congratulations! You can call yourself sailor code now! (Of course, there is only one true captain code). Submit this text to hackers@msvincognito.nl together with your code to crack it. Oh, and please keep the method to solve this secret (maybe using a mask? :) ) until the contest is over. Sincerely, your only captain code!";

		if (inputText.length() != messageLength) {
			throw new Exception(
					"Input text does not have the correct length to be masked by this image.");
		}

		// Add random characters in the places where there's no mask.
		char[] outputText = new char[mask.length];
		int maskCount = 0;
		for (int i = 0; i < outputText.length; i++) {
			if (mask[i]) {
				outputText[i] = inputText.charAt(maskCount);
				maskCount++;
			} else {
				int rnd = (int) (Math.random() * 52);
				char base = (rnd < 26) ? 'A' : 'a';
				outputText[i] = (char) (base + rnd % 26);
			}
		}

		// Apply shift cipher
		// outputText =
		// "@lkdo^qirEEi^oqqwFflkpVlGgKofrR`^BkUV?v`WcU`h^iGivAlJv^ropbpic@tp^fVilRo`OleAoaM>bkldtI%ILcN`lDr@opb)NTNMcquebohbAMLUufJpMDdlbfkQikwFDMvlkbqokrTb@N`N^@mq^fk`iIMkTSmFlJab&Qj+LPVdw`IPPr_`Ojf?qqoRefgpqburIqEqLltQGSLeok^At`hhbMoIp=jQpLmRsfkOj`orHldfhSskfVqGl`q+k`iprgNqh@ldbqebcDfoFtfqe@vHlroB?`lappStbmneitcqlln@kG`og^`_KKhfq+?`Le)Hu^ka>KwklmiQb`IUFt^JpbhbbCmqKebB>cTQj?MbqevlbaSQqGlpliiKsPgaWuuVfdboONqeFfC_QptQpb`obq%KDj^vLPCm_wbBrtOpfmIK_Rk`d^svnEjhps^sRph<w7&OIfUo&rMkqfiqeCb`?lkqbbMhGpKq>uwmVjfplEs@bo+P_PfaNk`bTuo^^bwPiv)BTK?`FvGlroTolkEivOCu>`^mq^fFka^m`llaQhQebaP".toCharArray();
		int shift = -3;
		for (int i = 0; i < outputText.length; i++) {
			outputText[i] = (char) (outputText[i] + shift);
		}

		String outputStr = new String(outputText);

		System.out.println("Output:");
		System.out.println(outputStr);

		// Now decode again to check
		char[] encoded = outputStr.toCharArray();
		for (int i = 0; i < encoded.length; i++) {
			encoded[i] = (char) (encoded[i] - shift);
		}
		StringBuilder builder = new StringBuilder(encoded.length);
		for (int i = 0; i < encoded.length; i++) {
			if (mask[i]) {
				builder.append(encoded[i]);
			}
		}

		System.out.println("Decoded:");
		System.out.println(builder);

	}

}