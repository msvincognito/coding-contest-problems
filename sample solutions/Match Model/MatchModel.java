package solutionCode;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MatchModel {
		private long[][][] a;
		
		public long solve(long n)
		{
			long k = 1;
			long s = 1;
			boolean flag = false;
			while (s<n)
			{
				k++;
				s = k*k*k;
			}
			a = new long[(int) k + 1][(int) k + 1][(int) k + 1];
			
			////////////////////////////////////////////////
			
			a[0][0][0] = 12;
			for (int i = 2;i<=k;i++)
				a[0][i-1][0] = 8;
			for (int j = 2; j <= k;j++)
				a[0][0][j-1] = 8;
			for (int i = 2;i<=k;i++)
				for (int j=2;j<=k;j++)
					a[0][i-1][j-1] = 5;
			
			/////////////////////////////////////////////////
			for (int m = 2;m<=k;m++)
			{
				a[m-1][0][0] = 8;
				for (int i=2;i<=k;i++)
					a[m-1][i-1][0] = 5;
				for (int j=2;j<=k;j++)
					a[m-1][0][j-1] = 5;
				for (int i=2;i<=k;i++)
					for (int j=2;j<=k;j++)
						a[m-1][i-1][j-1] = 3;
			}
			
			// 0,0,0 -> 12
			// 0,1,0 -> 8
			// 0,0,1 -> 8
			// 0,1,1 -> 5;
			// 1,0,0 -> 8
			// 1,1,0 -> 5
			// 1,0,1 -> 5
			// 1,1,1 -> 3
			long rez = 0;
			long count = 0;
			flag = true;
			for (int m=1;m<=k-1;m++)
				for (int i=1;i<=k-1;i++)
					for (int j=1;j<=k-1;j++)
					{
						rez = rez + a[m-1][i-1][j-1]; // 0,0,0 = 12
						count++;
					}
			long l = n - count;
			int j = (int) k;
			long p = 1;
			s = 1;
			while (s<l)
			{
				p++;
				s = p*p;
			}
			if (p>k-1)
				p = k-1;
			int i = 1;
			while (i<=p && flag)
			{
				int m = 1;
				while (m<=p && flag)
				{
					rez += a[m-1][i-1][j-1];
					count++;
					if (count == n)
						flag = false;
					m++;
				}
				i++;
			}
			
			l = n-count;
			i = (int) k;
			p = 1;
			s = 1;
			while (s<l)
			{
				p++;
				s = p*p;
			}
			if (p>k-1)
				p = k-1;
			j = 1;
			while (j<=p && flag)
			{
				long m = 1;
				while (m<=p &&  flag)
				{
					rez += a[(int) m-1][i-1][j-1];
					count++;
					if (count == n)
						flag = false;
					m++;
				}
				j++;
			}
			
			i = (int) k;
			j = (int) k;
			int m = 1;
			while (m<k && flag)
			{
				rez += a[m-1][i-1][j-1];
				count++;
				if (count == n)
					flag = false;
				m++;
			}
			
			l = n-count;
			m = (int) k;
			p = 1;
			s = 1;
			while (s<l)
			{
				p++;
				s = p*p;
			}
			i = 1;
			while (i<=p && flag)
			{
				j = 1;
				while (j<=p && flag)
				{
					rez += a[m-1][i-1][j-1];
					count++;
					if (count == n)
						flag = false;
					j++;
				}
				i++;
			}
			if (n==0)
				return 0;
			else
				return rez;
		}
		
		public static void main(String[] args) throws IOException {
			Scanner in = new Scanner(System.in);
			PrintWriter out = new PrintWriter(System.out);
			
			MatchModel main = new MatchModel();
			int n = in.nextInt();
			out.println(main.solve(n));
			
			out.flush();
		}
}
