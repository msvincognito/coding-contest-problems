import java.util.Scanner;

/**
 * 
 * Naive implementation for the Brainfuck generation problem. Takes the input through stdin and increments/decrements
 * one data cell and prints each character in that way.
 * 
 * @author Geert Konijnendijk
 * 
 */
public class BrainFuckGen {

	public static void main(String[] args) {

		// Read the input string
		Scanner s = new Scanner(System.in);
		String input = s.nextLine();

		String brainfuck = "";

		// Keep track of what character is currently under the data pointer
		char currentCell = 0;
		for (int i = 0; i < input.length(); i++) {
			char inputChar = input.charAt(i);

			// determine if the next character is above or below the current one in ASCII
			char brainfuckChar = '+';
			if (currentCell > inputChar)
				brainfuckChar = '-';

			// add or subtract the difference in the two characters to the final brainfuck code
			for (int j = 0; j < Math.abs(currentCell - inputChar); j++)
				brainfuck += brainfuckChar;

			// print the character
			brainfuck += '.';
			currentCell = inputChar;
		}

		System.out.println(brainfuck);
	}

}
