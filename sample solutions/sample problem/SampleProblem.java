
import java.util.Scanner;

public class SampleProblem{
	public static void main(String[] args){
		Scanner scanner=new Scanner(System.in);
		int sum=0, read=0;
		do {
			sum+=read;
			read=scanner.nextInt();
		}
		while(read>-1);
		System.out.println(sum);
	}
}

