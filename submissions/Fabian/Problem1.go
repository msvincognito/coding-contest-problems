// Fabian Fränz

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type character uint8

func main() {

	file, err := os.Open("sample input.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	first := true

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		testCase := scanner.Text()
		components := strings.Split(testCase, "|")
		numberOfSearchStrings := len(components) - 1
		outChans := make([]chan int, numberOfSearchStrings)
		for i := 0; i < len(outChans); i++ {
			outChans[i] = make(chan int)
		}
		inChan := make(chan character)
		nextInChan := make(chan character)
		go scanText(components[numberOfSearchStrings], (chan<- character)(inChan))
		for i := 0; i < numberOfSearchStrings; i++ {
			go findString(components[i], (<-chan character)(inChan), (chan<- character)(nextInChan), (chan<- int)(outChans[i]))
			inChan = nextInChan
			nextInChan = make(chan character)
		}
		go drop((<-chan character)(inChan))
		if !first {
			fmt.Println()
		}
		for i := 0; i < numberOfSearchStrings; i++ {
			result := <-outChans[i]
			fmt.Println(result)
		}
		first = false
	}
}

func findString(searchString string, inChannel <-chan character, outChannel chan<- character, resultChan chan<- int) {
	open := true
	var char character
	count := 0
	pos := 0
	for open {
		char, open = <-inChannel
		if open {
			outChannel <- char
			if character(searchString[pos]) == char {
				pos++
				if pos == len(searchString) {
					pos = 0
					count++
				}
			} else {
				pos = 0
			}
		} else {
			close(outChannel)
		}
	}
	resultChan <- count
}

func scanText(text string, outChannel chan<- character) {
	for i := 0; i < len(text); i++ {
		outChannel <- character(text[i])
	}
	close(outChannel)
}

func drop(inChannel <-chan character) {
	open := true
	for open {
		_, open = <-inChannel
	}
}
