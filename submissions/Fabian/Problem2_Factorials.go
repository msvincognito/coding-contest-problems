// Fabian Fränz

package main

import (
	"math"
	"fmt"
	"os"
	"strconv"
	"bufio"
)

type BigNumber []uint64

func MakeBigNumber(number uint64) BigNumber {
	b := make(BigNumber, 1)
	b[0] = number
	return b
}

func (b1 BigNumber) Add(b2 BigNumber) BigNumber {
	sum := make(BigNumber, 1)
	for i := 0; i < int(math.Max(float64(len(b1)), float64(len(b2)))); i++ {
		vb1 := uint64(0)
		vb2 := uint64(0)
		if i < len(b1) {
			vb1 = b1[i]
		}
		if i < len(b2) {
			vb2 = b2[i]
		}
		if !(i < len(sum)) {
			sum = append(sum, uint64(0))
		}
		capacity := 1000000000000000000 - vb1 - sum[i]
		if vb2 >= capacity {
			sum[i] = vb2 - capacity
			sum = append(sum, uint64(1))
		} else {
			sum[i] = sum[i] + vb1 + vb2
		}
	}
	return sum
}

func (b1 BigNumber) Mult(b2 BigNumber) BigNumber {
	var mult BigNumber
	var multiplier BigNumber
	if b1.SmallerThan(b2) {
		mult = b2
		multiplier = b1
	} else {
		mult = b1
		multiplier = b2
	}
	product := MakeBigNumber(uint64(0))
	for bi := MakeBigNumber(uint64(0)); bi.SmallerThan(multiplier); bi = bi.Add(MakeBigNumber(uint64(1))){
		product = product.Add(mult)
	}
	return product
}

func (b1 BigNumber) SmallerThan(b2 BigNumber) bool {
	if len(b1) < len(b2) {
		return true
	} else if len(b1) > len(b2) {
		return false
	} else {
		max := len(b2)-1
		return b1[max] < b2[max]
	}
}

func (b BigNumber) ToString() string {
	s := ""
	first := true
	for i := len(b)-1; i>=0; i--{
		if first {
			s = s+fmt.Sprintf("%d", b[i])
			first = false
		} else {
			s = s+fmt.Sprintf("%018d", b[i])
		}
	}
	return s
}

func factorial(k int) BigNumber {
	f := MakeBigNumber(uint64(1))
	for i:=1; i <= k; i++{
		f = f.Mult(MakeBigNumber(uint64(i)))
	}
	return f
}

func main() {

	file, err := os.Open("bigDataSetInput.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan() // skip first line containing the number of test cases
	for scanner.Scan() {
		testCase := scanner.Text()
		k, _ := strconv.ParseUint(testCase, 10, 64)
		fmt.Println(factorial(int(k)).ToString())
	}

}
