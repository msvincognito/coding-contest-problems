// Fabian Fränz

package main

import (
	"bufio"
	"math"
	"os"
	"strconv"
	"strings"
	"fmt"
)

var (
	primes []uint64 = []uint64{2, 3, 5, 7, 11, 13, 17}
)

func factorial(k uint64) uint64 {
	f := uint64(1)
	for i := uint64(1); i <= k; i++ {
		f = f * i
	}
	return f
}

func nOverK(n, k uint64) uint64 {
	return factorial(n) / (factorial(k) * factorial(n-k))
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan() // skip first line of input file
	for scanner.Scan() {
		testCase := scanner.Text()
		components := strings.Split(testCase, " ")
		interval_input, _ := strconv.ParseUint(components[0], 10, 64)
		p1_input, _ := strconv.ParseUint(components[1], 10, 64)
		p2_input, _ := strconv.ParseUint(components[2], 10, 64)
		intervals := uint64(interval_input)
		p1 := float64(p1_input) / 100.0
		p2 := float64(p2_input) / 100.0
		probability1 := float64(0.0)
		probability2 := float64(0.0)
		for _,prime := range primes {
			if prime <= intervals {
				probability1 += float64(nOverK(intervals, prime)) * math.Pow(p1, float64(prime)) * math.Pow(1.0-p1, float64(intervals-prime))
				probability2 += float64(nOverK(intervals, prime)) * math.Pow(p2, float64(prime)) * math.Pow(1.0-p2, float64(intervals - prime))
			}
		}
		finalProbability := 1 - (1-probability1) * (1-probability2)
		fmt.Printf("%.4f\n", finalProbability)
	}
}
