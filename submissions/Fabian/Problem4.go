// Fabian Fränz

package main

import (
	"fmt"
	"strconv"
	"bufio"
	"os"
	"math"
)

func main() {
	file, err := os.Open("../../problem input/Match Model/sample input.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan() // skip first line of input file
	for scanner.Scan() {
		testCase := scanner.Text()
		noc, _ := strconv.ParseInt(testCase, 10, 64)
		numberOfMatches := 0
		numberOfCubes := int(noc)
		if numberOfCubes > 0 {
			numberOfMatches = 12
		}
		currentCubic := 1
		toggle := true
		counter := 0
		secondCounter := 0
		cap := 1
		stepsSince8 := 0
		for i := 2; i <= numberOfCubes; i++{
			if i >= int(math.Pow((float64)(currentCubic+1), 3)) {
				currentCubic++
			}
			if i == int(math.Pow((float64)(currentCubic), 3)+1) {
				numberOfMatches += 8
				stepsSince8 = 0
			} else if i == int(math.Pow((float64)(currentCubic), 3) + math.Pow((float64)(currentCubic), 2) + 1) {
				numberOfMatches += 8
				stepsSince8 = 0
			} else if i == int(math.Pow((float64)(currentCubic), 3) + math.Pow((float64)(currentCubic), 2) + float64(currentCubic*(currentCubic+1)) + 1) {
				numberOfMatches += 8
				stepsSince8 = 0
			} else if stepsSince8 < 2 {
				numberOfMatches += 5
				toggle = true
				cap = 1
				counter = 0
				secondCounter = 0
				stepsSince8++
			} else if toggle {
				numberOfMatches += 3
				counter++
				if counter >= cap {
					toggle = !toggle
					counter = 0
				}
			} else {
				numberOfMatches += 5
				toggle = !toggle
				secondCounter++
				if secondCounter >= 2 {
					secondCounter = 0
					cap++
				}
			}

		}
		fmt.Println(numberOfMatches)
	}
}
