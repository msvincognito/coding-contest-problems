package main

import (
	"fmt"
	"math"
	"runtime"
	"sort"
)

type UDN uint32

// type encapsulates a list of UDN, to allow sorting with the sort library
type UDNList []UDN

// sort interface functions
func (ul UDNList) Len() int {
	return len(ul)
}

func (ul UDNList) Less(i, j int) bool {
	return ul[i] < ul[j]
}

func (ul UDNList) Swap(i, j int) {
	ul[i], ul[j] = ul[j], ul[i]
}

// checks if the given UDN is a valid udn by looking for that specific UDN in the sorted list of all possible UDNs
func isUDN(udnList UDNList, udn UDN) bool {
	index := sort.Search(len(udnList), func(i int) bool {
		return udnList[i] >= udn
	})
	if index >= len(udnList) {
		return false
	} else if udn != udnList[index] {
		return false
	} else {
		return true
	}
}

// factorize UDN into UDN factors
func factorizeUDN(udnList UDNList, udn UDN) []UDN {
	udnRest := udn
	var factors []UDN
	for i := 0; i < len(udnList) && uint32(udnList[i]) <= uint32(math.Sqrt((float64)(udnRest))); i++ {
		if udnRest%udnList[i] == 0 {
			nextUDN := udnRest / udnList[i]
			if isUDN(udnList, nextUDN) {
				udnRest = nextUDN
				factors = append(factors, udnList[i])
			}
		}
	}
	factors = append(factors, udnRest)
	return factors
}

// worker which search a subset of UDNs and returns the UDN with the highest factorisation
func findBestUDN(udnList UDNList, min, max int, best chan UDN) {
	maxFactorCount := 0
	bestUDN := UDN(0)
	var udn UDN
	for i := min; i <= max; i++ {
		udn = udnList[i]
		factors := factorizeUDN(udnList, udn)
		if len(factors) > maxFactorCount {
			maxFactorCount = len(factors)
			bestUDN = udn
		}
	}
	best <- bestUDN
}

// creates the recursive function handle to generate all possible UDNs
func createUDNCreationFunction() func(chan UDN, UDN, map[int]bool) {
	var udnFunction func(chan UDN, UDN, map[int]bool)
	referenceCounter := 0
	udnFunction = func(udnChannel chan UDN, lastUDN UDN, digits map[int]bool) {
		for i := 0; i < 10; i++ {
			_, exists := digits[i]
			if (lastUDN != 0 || i != 0) && !exists {
				nextDigits := make(map[int]bool)
				for k, v := range digits {
					nextDigits[k] = v
				}
				nextDigits[i] = true
				udn := uint64(lastUDN)*uint64(10) + uint64(i)
				if udn <= 987654321 {
					go udnFunction(udnChannel, UDN(udn), nextDigits)
					referenceCounter++
					udnChannel <- UDN(udn)
				}
			}
		}
		referenceCounter--
		if referenceCounter == 0 {
			close(udnChannel)
		}
	}

	return udnFunction
}

func main() {

	udnChannel := make(chan UDN, 1000)
	channels := make([]chan UDN, 10)
	for i := 0; i < 10; i++ {
		channels[i] = make(chan UDN)
	}
	udns := createUDNCreationFunction()
	// creating all UDNs
	fmt.Println("Gathering UDNs ...")
	go udns(udnChannel, UDN(0), make(map[int]bool))

	var udnList UDNList
	open := true
	var udn UDN
	for open {
		udn, open = <-udnChannel
		if open {
			udnList = append(udnList, udn)
		}
	}
	// sorting the UDNs
	sort.Sort(udnList)

	fmt.Printf("Aquired %d UDNs\n", len(udnList))

	runtime.GOMAXPROCS(runtime.NumCPU()) // using as much processing power as available

	length := len(udnList) - 1
	workers := runtime.NumCPU()
	fmt.Printf("Found %d available cores\n", workers)
	fmt.Printf("Starting %d workers ...\n", workers)
	for i := 0; i < workers; i++ {
		min := int(float32(length) * (float32(i) / float32(workers)))
		max := int(float32(length) * (float32(i+1) / float32(workers)))
		go findBestUDN(udnList, min, max, channels[i])
		fmt.Printf("Worker %d started\n", i)
	}

	bestUDN := UDN(0)
	maxFactorCount := 0
	for i := 0; i < workers; i++ {
		udn := <-channels[i]
		fmt.Printf("Worker %d is done\n", i)
		factors := factorizeUDN(udnList, udn)
		if len(factors) > maxFactorCount {
			bestUDN = udn
			maxFactorCount = len(factors)
		}
	}
	factors := factorizeUDN(udnList, bestUDN)
	fmt.Printf("\nResult\n")
	fmt.Printf("Best UDN: %d\n", bestUDN)
	fmt.Printf("Factors: ")
	fmt.Print(factors)
	fmt.Println()
	fmt.Printf("Count: %d\n", len(factors))
}
