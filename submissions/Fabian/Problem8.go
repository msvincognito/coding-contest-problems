// Fabian Fränz

// Judging: Produces 4972 characters

package main

import (
	"fmt"
	"os"
	"bufio"
	"sort"
)

const (
	NUMBER_OF_STRINGS = 10
	INTERPRET = false
)

func interpretBrainfuck(code string) {
	codePosition := 0
	ptr := 0;
	memory := make([]int8, 10000)
	for codePosition < len(code) {
		token := string(code[codePosition])
		switch token {
		case ">":
			ptr++
		case "<":
			ptr--
		case "+":
			memory[ptr]++
		case "-":
			memory[ptr]--
		case ".":
			fmt.Print(string(memory[ptr]))
		case ",":
			panic("Unsupported")
		case "[":
			if memory[ptr] == 0 {
				otherBrackets := 0
				codePosition++
				t := string(code[codePosition])
				for ; t != "]" || otherBrackets != 0; t = string(code[codePosition]) {
					if t == "[" {
						otherBrackets++
					} else if t == "]" {
						otherBrackets--
					}
					codePosition++
				}
			}
		case "]":
			if memory[ptr] != 0 {
				otherBrackets := 0
				codePosition--
				t := string(code[codePosition])
				for ; t != "[" || otherBrackets != 0; t = string(code[codePosition]) {
					if t == "]" {
						otherBrackets++
					} else if t == "[" {
						otherBrackets--
					}
					codePosition--
				}
			}
		}
		codePosition++;
	}
	fmt.Println()
}

type Brainfucker interface {
	setUp()
	prepare(string)
	fuckBrain(string)string
	cleanUp()
}

type NaiveBrainfucker struct {

}

func (bf NaiveBrainfucker) setup() {

}

func (bf NaiveBrainfucker) prepare(input string) {

}

func (bf NaiveBrainfucker) fuckBrain(input string) string {
	lastChar := uint8(0)
	brainfuck := ""
	for i := 0; i < len(input); i++{
		c := input[i]
		if c > lastChar {
			for lastChar < c{
				brainfuck += "+"
				lastChar++
			}
		} else if c < lastChar {
			for lastChar > c {
				brainfuck += "-"
				lastChar--
			}
		}
		brainfuck += "."
	}
	return brainfuck
}

func (bf NaiveBrainfucker) cleanUp() {

}

type MovingBrainFucker struct {
	charMap map[string]int
}

func (bf MovingBrainFucker) setup() {

}

func (bf MovingBrainFucker) prepare(input string) {
	var bytes []int
	for _, c := range input{
		bytes = append(bytes, int(c))
	}
	sort.Ints(bytes)
	// TODO create table
}

func (bf MovingBrainFucker) fuckBrain(input string) string {
	// TODO implement
	return ""
}

func (bf MovingBrainFucker) cleanUp() {

}

func main() {
	fucker := new(NaiveBrainfucker)
	fucker.setup()
	for i := 0; i < NUMBER_OF_STRINGS; i++ {
		input := ""
		bio := bufio.NewReader(os.Stdin)
		for {
			line, hasMoreInLine, err := bio.ReadLine()
			if err != nil {
				panic(err)
			}
			for _, b := range line {
				input += string(b)
			}
			if !hasMoreInLine {
				break;
			}
		}
		fucker.prepare(input)
		brainfuck := fucker.fuckBrain(input)
		if INTERPRET {
			interpretBrainfuck(brainfuck)
		} else {
			fmt.Println(brainfuck)
		}
		fucker.cleanUp()
	}
}
