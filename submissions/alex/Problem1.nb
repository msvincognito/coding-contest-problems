(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     15405,        347]
NotebookOptionsPosition[     14666,        319]
NotebookOutlinePosition[     15009,        334]
CellTagsIndexPosition[     14966,        331]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Awesome", " ", "import"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"data", " ", "=", " ", 
    RowBox[{"StringSplit", "[", 
     RowBox[{
      RowBox[{"StringSplit", "[", 
       RowBox[{
        RowBox[{"Import", "[", 
         RowBox[{
         "\"\<C:\\\\Users\\\\I6046653\\\\strings.txt\>\"", ",", " ", 
          "\"\<Text\>\""}], "]"}], ",", "\"\<\\n\>\""}], "]"}], ",", " ", 
      "\"\<|\>\""}], "]"}]}], ";"}]}]], "Input",
 CellChangeTimes->{{3.610790893632777*^9, 3.610790934923501*^9}, {
   3.610790999444956*^9, 3.6107910117767725`*^9}, {3.6107911102801027`*^9, 
   3.6107911107637153`*^9}, {3.6107911758039017`*^9, 3.6107911942903757`*^9}, 
   3.610791228689088*^9, {3.6107913039631367`*^9, 3.6107913069140935`*^9}, {
   3.6107914783442955`*^9, 3.610791478952707*^9}, {3.6107919076620517`*^9, 
   3.610791944523885*^9}, 3.610792318428027*^9, {3.610792715998032*^9, 
   3.610792725420674*^9}, {3.6108025514840264`*^9, 3.610802555628441*^9}, {
   3.6108025953984175`*^9, 3.6108026103819156`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"For", "[", 
    RowBox[{
     RowBox[{"index", " ", "=", " ", "1"}], ",", " ", 
     RowBox[{"index", " ", "\[LessEqual]", " ", 
      RowBox[{"Length", "[", "data", "]"}]}], ",", " ", 
     RowBox[{"index", "++"}], ",", 
     RowBox[{
      RowBox[{"currentList", " ", "=", " ", 
       RowBox[{"Part", "[", 
        RowBox[{"data", ",", " ", "index"}], "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
       "Set", " ", "up", " ", "the", " ", "list", " ", "of", " ", "search", 
        " ", "for", " ", "strings"}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"searchStrings", " ", "=", " ", 
       RowBox[{"Part", "[", 
        RowBox[{"currentList", ",", 
         RowBox[{"1", ";;", " ", 
          RowBox[{
           RowBox[{"Length", "[", "currentList", "]"}], "-", "1"}]}]}], 
        "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
       "Set", " ", "up", " ", "the", " ", "string", " ", "we", " ", "iterate",
         " ", "through"}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"originString", " ", "=", " ", 
       RowBox[{"Part", "[", 
        RowBox[{"currentList", ",", " ", 
         RowBox[{"Length", "[", "currentList", "]"}]}], "]"}]}], ";", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Store", " ", "our", " ", "results", " ", "in", " ", 
        RowBox[{"here", " ", ":", "D"}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"results", " ", "=", " ", 
       RowBox[{"ConstantArray", "[", 
        RowBox[{"0", ",", " ", 
         RowBox[{"Length", "[", "searchStrings", "]"}]}], "]"}]}], ";", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
        RowBox[{
        "Set", " ", "first", " ", "string", " ", "equal", " ", "to", " ", 
         "the", " ", "first", " ", "n", " ", "chars", " ", "of", " ", "the", 
         " ", "originString"}], ",", " ", 
        RowBox[{
        "where", " ", "n", " ", "is", " ", "the", " ", "longest", " ", 
         "search", " ", "string", " ", "length"}]}], " ", "*)"}], 
      "\[IndentingNewLine]", 
      RowBox[{"For", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"x", "=", " ", "1"}], ";", " ", 
         RowBox[{"maxSSLength", " ", "=", " ", "0"}]}], ",", " ", 
        RowBox[{"x", " ", "<=", 
         RowBox[{"Length", "[", "searchStrings", "]"}]}], ",", " ", 
        RowBox[{"x", "++"}], ",", " ", 
        RowBox[{
         RowBox[{"If", "[", " ", 
          RowBox[{
           RowBox[{
            RowBox[{"StringLength", "[", 
             RowBox[{"Part", "[", 
              RowBox[{"searchStrings", ",", " ", "x"}], "]"}], " ", "]"}], 
            " ", ">", " ", "maxSSLength"}], ",", " ", 
           RowBox[{"maxSSLength", " ", "=", " ", 
            RowBox[{"StringLength", "[", 
             RowBox[{"Part", "[", 
              RowBox[{"searchStrings", ",", " ", "x"}], "]"}], " ", "]"}]}]}],
           "]"}], ";"}]}], "]"}], ";", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
        RowBox[{
        "We", " ", "now", " ", "have", " ", "the", " ", "length", " ", "of", 
         " ", "the", " ", "longest", " ", "search", " ", 
         RowBox[{"string", ".", " ", "The"}], " ", "search", " ", "strings"}],
         ",", " ", 
        RowBox[{"and", " ", "the", " ", "origin", " ", 
         RowBox[{"string", ".", " ", "\[IndentingNewLine]", "Now"}], " ", 
         "we", " ", "create", " ", "our", " ", "initial", " ", "string", " ", 
         "to", " ", "check", " ", 
         RowBox[{"against", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"currentSearchString", " ", "=", " ", 
       RowBox[{"StringTake", "[", 
        RowBox[{"originString", ",", "maxSSLength"}], "]"}]}], ";", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{
        RowBox[{
        "For", " ", "every", " ", "character", " ", "in", " ", "the", " ", 
         "search", " ", 
         RowBox[{"string", ".", " ", "THIS"}], " ", "IS", " ", "THE", " ", 
         "ITERATION"}], " ", "-", " ", 
        RowBox[{"once", " ", "per", " ", "char"}]}], " ", "*)"}], 
      "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"For", "[", 
        RowBox[{
         RowBox[{"currentIndex", "  ", "=", " ", "maxSSLength"}], ",", " ", 
         RowBox[{"currentIndex", "  ", "<", " ", 
          RowBox[{"StringLength", "[", "originString", "]"}]}], ",", " ", 
         RowBox[{"currentIndex", "++"}], ",", "\[IndentingNewLine]", 
         "\[IndentingNewLine]", 
         RowBox[{"(*", " ", 
          RowBox[{
           RowBox[{
           "Check", " ", "for", " ", "a", " ", "match", " ", "between", " ", 
            "the", " ", "current", " ", "string", " ", "and", " ", "the", " ",
             "search", " ", "strings"}], ",", " ", 
           RowBox[{"for", " ", "each", " ", 
            RowBox[{"string", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"Do", "[", 
           RowBox[{
            RowBox[{"If", "[", 
             RowBox[{
              RowBox[{"StringMatchQ", "[", 
               RowBox[{"i", ",", 
                RowBox[{"StringTake", "[", 
                 RowBox[{"currentSearchString", ",", " ", 
                  RowBox[{"StringLength", "[", "i", "]"}]}], "]"}]}], "]"}], 
              ",", " ", 
              RowBox[{
               RowBox[{"results", "[", 
                RowBox[{"[", 
                 RowBox[{"Part", "[", 
                  RowBox[{
                   RowBox[{"Part", "[", 
                    RowBox[{
                    RowBox[{"Position", "[", 
                    RowBox[{"searchStrings", ",", " ", "i"}], "]"}], ",", 
                    "1"}], "]"}], ",", "1"}], "]"}], "]"}], "]"}], " ", "=", 
               " ", 
               RowBox[{
                RowBox[{"results", "[", 
                 RowBox[{"[", 
                  RowBox[{"Part", "[", 
                   RowBox[{
                    RowBox[{"Part", "[", 
                    RowBox[{
                    RowBox[{"Position", "[", 
                    RowBox[{"searchStrings", ",", " ", "i"}], "]"}], ",", 
                    "1"}], "]"}], ",", "1"}], "]"}], "]"}], "]"}], " ", "+", 
                "1"}]}]}], "]"}], ",", "\[IndentingNewLine]", 
            RowBox[{"{", 
             RowBox[{"i", ",", "searchStrings"}], "}"}]}], "]"}], ";", 
          "\[IndentingNewLine]", "\[IndentingNewLine]", 
          RowBox[{"(*", " ", 
           RowBox[{
           "The", " ", "characters", " ", "match", " ", "for", " ", "that", 
            " ", 
            RowBox[{"string", ".", " ", "Then"}], " ", "add", " ", "one", " ",
             "to", " ", "the", " ", "list", " ", "of", " ", "matches"}], " ", 
           "*)"}], "\[IndentingNewLine]", 
          RowBox[{"(*", " ", 
           RowBox[{
            RowBox[{
            "Finish", " ", "by", " ", "deleting", " ", "the", " ", "current", 
             " ", "character"}], ",", " ", 
            RowBox[{
            "and", " ", "adding", " ", "the", " ", "next", " ", "one"}]}], 
           " ", "*)"}], "\[IndentingNewLine]", 
          RowBox[{"currentSearchString", " ", "=", " ", 
           RowBox[{"StringInsert", "[", 
            RowBox[{
             RowBox[{"StringDrop", "[", 
              RowBox[{"currentSearchString", ",", "1"}], "]"}], ",", " ", 
             RowBox[{"StringTake", "[", 
              RowBox[{"originString", ",", " ", 
               RowBox[{"{", 
                RowBox[{"currentIndex", "+", "1"}], "}"}]}], "]"}], ",", " ", 
             RowBox[{"-", "1"}]}], "]"}]}], ";"}]}], "]"}], 
       "\[IndentingNewLine]", 
       RowBox[{"For", "[", 
        RowBox[{
         RowBox[{"b", " ", "=", " ", "1"}], ",", " ", 
         RowBox[{"b", " ", "\[LessEqual]", "  ", 
          RowBox[{"Length", "[", "results", "]"}]}], ",", " ", 
         RowBox[{"b", "++"}], ",", " ", 
         RowBox[{"Print", "[", 
          RowBox[{"Part", "[", 
           RowBox[{"results", ",", " ", "b"}], "]"}], "]"}]}], "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"Print", "[", "\"\<\\n\>\"", "]"}], ";"}]}], "]"}], ";"}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.610791965865779*^9, 3.610792268101136*^9}, {
   3.610792303857253*^9, 3.6107923278818693`*^9}, {3.610792402240882*^9, 
   3.610792697870367*^9}, {3.61079274164509*^9, 3.610792805584234*^9}, {
   3.6107928438993273`*^9, 3.610792845381365*^9}, {3.610792893499501*^9, 
   3.6107929430307713`*^9}, {3.610792998084583*^9, 3.610793087057268*^9}, {
   3.610793119141697*^9, 3.6107931658504996`*^9}, {3.610793211329976*^9, 
   3.6107932297852497`*^9}, {3.610793262772419*^9, 3.6107932726630096`*^9}, {
   3.610793316172246*^9, 3.6107933364682364`*^9}, {3.6107933918493013`*^9, 
   3.610793424828336*^9}, {3.6107934666536517`*^9, 3.61079359345289*^9}, {
   3.610793645807497*^9, 3.610793699222924*^9}, {3.610793735977231*^9, 
   3.610793797754419*^9}, {3.61079413217805*^9, 3.6107942016773863`*^9}, {
   3.6107991174085875`*^9, 3.6107993178256416`*^9}, {3.610799351369893*^9, 
   3.610799423037671*^9}, {3.6107995923255367`*^9, 3.610799618440439*^9}, {
   3.6108000022763567`*^9, 3.6108000061453056`*^9}, {3.6108000506538177`*^9, 
   3.610800117611593*^9}, {3.6108001520731125`*^9, 3.610800241713586*^9}, {
   3.610800374068228*^9, 3.6108003953005085`*^9}, {3.6108004332095547`*^9, 
   3.6108006061856813`*^9}, {3.6108007964155455`*^9, 3.610801036525869*^9}, {
   3.6108011150165358`*^9, 3.610801238789316*^9}, {3.610801281424936*^9, 
   3.610801378661606*^9}, {3.610801423481268*^9, 3.610801469954561*^9}, 
   3.6108015342121973`*^9, {3.6108016072996025`*^9, 3.610801737484106*^9}, {
   3.610801789479906*^9, 3.6108018085902734`*^9}, {3.61080185706069*^9, 
   3.61080185888603*^9}, {3.6108019196527042`*^9, 3.6108019807314024`*^9}, {
   3.6108020179402647`*^9, 3.6108021283967605`*^9}, {3.610802193311771*^9, 
   3.6108022444505377`*^9}, {3.6108022832652073`*^9, 3.610802283553236*^9}, {
   3.610802322642144*^9, 3.6108023377536554`*^9}, {3.610803099407959*^9, 
   3.610803099627963*^9}, {3.6108033323247814`*^9, 3.610803424272949*^9}, {
   3.6108043399909563`*^9, 3.6108043403341627`*^9}, {3.6108044108952465`*^9, 
   3.610804617334016*^9}, {3.610804656989979*^9, 3.610804667145774*^9}, {
   3.6108047502797728`*^9, 3.6108047577367163`*^9}, {3.6108047954738417`*^9, 
   3.6108048070336637`*^9}, {3.6108048392482834`*^9, 
   3.6108049323976746`*^9}, {3.6108049856525126`*^9, 
   3.6108049995523796`*^9}, {3.610805040041864*^9, 3.6108050689804206`*^9}, {
   3.6108051115068383`*^9, 3.61080521195717*^9}, {3.6108052682430525`*^9, 
   3.610805272408333*^9}, {3.6108053442221217`*^9, 3.6108054385571356`*^9}, {
   3.610805471707773*^9, 3.610805592781701*^9}, {3.6108056783849487`*^9, 
   3.610805694453258*^9}, 3.6108058014557157`*^9, {3.6108058765711603`*^9, 
   3.6108058782871933`*^9}, {3.6108059588487825`*^9, 
   3.6108061192626114`*^9}, {3.61080614984208*^9, 3.6108061800028276`*^9}, {
   3.610806213798561*^9, 3.610806217904072*^9}}],

Cell[CellGroupData[{

Cell[BoxData["1"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["1"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["0"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["1"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["\<\"\\n\"\>"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["2"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["0"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["0"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["1"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}],

Cell[BoxData["\<\"\\n\"\>"], "Print",
 CellChangeTimes->{{3.6108061195278254`*^9, 3.610806134514594*^9}, {
   3.610806174321636*^9, 3.6108061819373264`*^9}, 3.6108062185618057`*^9}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1264, 889},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 1085, 22, 52, "Input"],
Cell[CellGroupData[{
Cell[1667, 46, 11189, 227, 632, "Input"],
Cell[CellGroupData[{
Cell[12881, 277, 171, 2, 23, "Print"],
Cell[13055, 281, 171, 2, 23, "Print"],
Cell[13229, 285, 171, 2, 23, "Print"],
Cell[13403, 289, 171, 2, 23, "Print"],
Cell[13577, 293, 181, 2, 43, "Print"],
Cell[13761, 297, 171, 2, 23, "Print"],
Cell[13935, 301, 171, 2, 23, "Print"],
Cell[14109, 305, 171, 2, 23, "Print"],
Cell[14283, 309, 171, 2, 23, "Print"],
Cell[14457, 313, 181, 2, 43, "Print"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
