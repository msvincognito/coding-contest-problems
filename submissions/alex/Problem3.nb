(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     22949,        511]
NotebookOptionsPosition[     22395,        488]
NotebookOutlinePosition[     22738,        503]
CellTagsIndexPosition[     22695,        500]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
   RowBox[{
   "Alex", " ", "Stilwell", " ", "\[IndentingNewLine]", "\[IndentingNewLine]",
     "RUNNING", " ", 
    RowBox[{"INSTRUCTIONS", ":", " ", 
     RowBox[{"I", " ", 
      RowBox[{"don", "'"}], "t", " ", "assume", " ", "that", " ", "you", " ", 
      "know", " ", "how", " ", "to", " ", "use", " ", "Mathematica"}]}]}], 
   ",", " ", 
   RowBox[{"but", " ", "because", " ", 
    RowBox[{"I", "'"}], "m", " ", "nice"}], ",", " ", 
   RowBox[{
   "here", " ", "is", " ", "how", " ", "to", " ", "run", " ", "the", " ", 
    RowBox[{"program", ":", " ", 
     RowBox[{
      RowBox[{
      "change", " ", "the", " ", "path", " ", "in", " ", "the", " ", "Import",
        " ", "below", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"the", " ", "bit", " ", "next", " ", "to", " ", "DataList"}],
          " ", "="}], " ", ")"}], " ", "for", " ", "your", " ", "own", " ", 
       "test", " ", 
       RowBox[{"cases", ".", "\[IndentingNewLine]", "Click"}], " ", "on", " ",
        "Evaluation"}], " ", "-", " ", 
      RowBox[{"Evaluate", " ", 
       RowBox[{"Notebook", ".", " ", "When"}], " ", "the", " ", "run", " ", 
       "is", " ", "complete"}]}]}]}], ",", " ", 
   RowBox[{
   "results", " ", "shall", " ", "appear", " ", "at", " ", "the", " ", 
    "bottom", "\[IndentingNewLine]", "of", " ", "this", " ", "notebook"}], 
   ",", " ", 
   RowBox[{
   "you", " ", "should", " ", "automatically", " ", "go", " ", "to", " ", 
    RowBox[{"them", ".", " ", "If"}], " ", "not"}], ",", " ", 
   RowBox[{
    RowBox[{"scroll", " ", 
     RowBox[{"down", ".", "\[IndentingNewLine]", "Should"}], " ", "the", " ", 
     "run", " ", "continue", " ", "infinitely", " ", 
     RowBox[{"(", 
      RowBox[{
      "and", " ", "you", " ", "can", " ", "see", " ", "this", " ", "by", " ", 
       "ANY", " ", "of", " ", "the", " ", "bars", " ", "on", " ", "the", " ", 
       "right", " ", "remaining", " ", "black"}], ")"}], 
     "\[IndentingNewLine]", "you", " ", "should", " ", "click", " ", 
     "Evaluation"}], " ", "\[Rule]", " ", 
    RowBox[{"Abort", " ", 
     RowBox[{"Evaluation", ".", "\[IndentingNewLine]", "Also"}]}]}], ",", " ", 
   RowBox[{
    RowBox[{
    "noone", " ", "cares", " ", "if", " ", "n", " ", "is", " ", "small", " ", 
     "or", " ", 
     RowBox[{"not", "."}]}], " ", ":", 
    RowBox[{
     RowBox[{
     "D", "\[IndentingNewLine]", "\[IndentingNewLine]", "Because", " ", "we", 
      " ", "are", " ", "exceptionally", " ", "lazy", " ", "Mathematica", " ", 
      "is", " ", "the", " ", "tool", " ", "of", " ", 
      RowBox[{"choice", ".", " ", "No"}], " ", "point", "\[IndentingNewLine]",
       "pissing", " ", "about", " ", "in", " ", "Java", " ", "because", " ", 
      "it", " ", "would", " ", "be", " ", "a", " ", "bit", " ", "like", " ", 
      "bringing", " ", "a", " ", "knife", " ", "to", " ", "a", " ", "gun", 
      " ", 
      RowBox[{"fight", ".", "\[IndentingNewLine]", "However"}]}], " ", "-", 
     " ", 
     RowBox[{
     "now", " ", "that", " ", "I", " ", "have", " ", "said", " ", "that", " ",
       "no", " ", "doubt", " ", "my", " ", "program", " ", "shall", " ", 
      "inveitably", " ", "not", " ", 
      RowBox[{"work", ".", " ", "Oh"}], " ", 
      RowBox[{
      "well", ".", "\[IndentingNewLine]", "\[IndentingNewLine]", "This"}], 
      " ", "notebook", " ", "will", " ", "take", " ", "a", " ", "file", " ", 
      "of", " ", "data"}]}]}], ",", " ", 
   RowBox[{"in", " ", 
    RowBox[{"format", ":", "\[IndentingNewLine]", 
     RowBox[{
     "a", "\[IndentingNewLine]", "n", " ", "p", " ", "r", 
      "\[IndentingNewLine]", "n", " ", "p", " ", "r", "\[IndentingNewLine]", 
      "where", " ", "a", " ", "is", " ", "the", " ", "number", " ", "of", " ",
       "n", " ", "p", " ", "r", " ", 
      RowBox[{"repititions", ".", " ", "n"}], " ", "is", " ", "the", " ", 
      "number", " ", "of", " ", 
      RowBox[{"intervals", ".", " ", "\[IndentingNewLine]", "p"}], " ", "is", 
      " ", "the", " ", "p", 
      RowBox[{"(", 
       RowBox[{"t1Scores", " ", "in", " ", "an", " ", "interval"}], ")"}], 
      " ", "and", " ", "r", " ", "is", " ", "the", " ", "p", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"t2Scores", " ", "in", " ", "an", " ", "interval"}], ")"}], 
       ".", "\[IndentingNewLine]", "If"}], " ", "then", " ", "calculates", 
      " ", "the", " ", "probability", " ", "that", " ", "at", " ", "least", 
      " ", "one", " ", "team", " ", "scores", " ", "a", " ", "prime", " ", 
      "numbers", "\[IndentingNewLine]", "of", " ", "goals", " ", "overall", 
      " ", "during", " ", "the", " ", 
      RowBox[{"game", "."}]}]}]}]}], " ", "\[IndentingNewLine]", 
  "\[IndentingNewLine]", "*)"}]], "Input",
 CellChangeTimes->{{3.610532346858317*^9, 3.6105323482163167`*^9}, {
  3.610535316258317*^9, 3.6105353328823166`*^9}, {3.6105355679933167`*^9, 
  3.6105356179763165`*^9}, {3.610535671147317*^9, 3.6105357809553165`*^9}, {
  3.610535876760317*^9, 3.610535971758317*^9}, {3.6105360088143167`*^9, 
  3.6105360205053167`*^9}, {3.610536062593317*^9, 3.610536090872317*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "First", " ", "off", " ", "lets", " ", "write", " ", "a", " ", "silly", 
     " ", "little", " ", "import", " ", 
     RowBox[{"method", ".", " ", "\[IndentingNewLine]", 
      RowBox[{"NOTE", ":", " ", 
       RowBox[{
       "When", " ", "you", " ", "run", " ", "this", " ", "on", " ", "your", 
        " ", "own", " ", "file"}]}]}]}], ",", " ", 
    RowBox[{
     RowBox[{
     "you", " ", "should", " ", "delete", " ", "my", " ", "path", 
      "\[IndentingNewLine]", "and", " ", "use", " ", "insert"}], " ", 
     "\[Rule]", " ", 
     RowBox[{"filePath", " ", "on", " ", "the", " ", "menu"}]}], ",", " ", 
    RowBox[{
    "then", " ", "select", " ", "the", " ", "text", " ", "file", " ", "you", 
     " ", "are", " ", 
     RowBox[{"using", ".", " ", "\[IndentingNewLine]", "This"}], " ", "will", 
     " ", "save", " ", "you", " ", "the", " ", "headache", " ", "of", " ", 
     "writing", " ", "the", " ", "path", " ", 
     RowBox[{"out", "."}]}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"DataList", " ", "=", " ", 
    RowBox[{"Import", "[", 
     RowBox[{
     "\"\<\\\\\\\\Client\\\\C$\\\\Desktop\\\\textInput.txt\>\"", ",", 
      "\"\<Table\>\""}], "]"}]}], ";"}]}]], "Input",
 CellChangeTimes->{{3.610532350465317*^9, 3.6105323733113165`*^9}, 
   3.6105324411463165`*^9, {3.610532476581317*^9, 3.6105325018303165`*^9}, {
   3.610532555303317*^9, 3.610532604270317*^9}, {3.6105326849603167`*^9, 
   3.6105327038703165`*^9}, {3.6105327354603167`*^9, 3.610532796494317*^9}, 
   3.6105333180213165`*^9, {3.6105353827423167`*^9, 3.610535385237317*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"numberOfRuns", " ", "=", "  ", 
    RowBox[{"Part", "[", 
     RowBox[{"DataList", ",", "1", ",", "1"}], "]"}]}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "sets", " ", "how", " ", "many", " ", "times", " ", "our", " ", "splendid",
     " ", "program", " ", "shall", " ", "run"}], " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.6105326761103168`*^9, 3.6105326806863165`*^9}, {
   3.6105328019673166`*^9, 3.6105328109723167`*^9}, {3.610532898418317*^9, 
   3.6105329547123165`*^9}, {3.610532989576317*^9, 3.610533021207317*^9}, {
   3.6105330685123167`*^9, 3.6105330907833166`*^9}, {3.610533322287317*^9, 
   3.610533322918317*^9}, 3.6105335690853167`*^9, {3.6105337174363165`*^9, 
   3.610533723645317*^9}, {3.6105339673083167`*^9, 3.610533970307317*^9}, {
   3.610535403020317*^9, 3.610535414982317*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "The", " ", "forumla", " ", "below", " ", "defines", " ", "a", " ", 
     RowBox[{"function", ".", " ", "What"}], " ", "this", " ", "function", 
     " ", "says", " ", 
     RowBox[{"is", ":", " ", 
      RowBox[{"for", " ", "some", " ", "3", " ", "variables"}]}]}], ",", " ", 
    RowBox[{
    "calculate", " ", "the", " ", "probability", "\[IndentingNewLine]", 
     "that", " ", "x", " ", "is", " ", "equal", " ", "to", " ", "i"}], ",", 
    " ", 
    RowBox[{
    "in", " ", "the", " ", "binomial", " ", "distribution", " ", "of", " ", 
     "n"}], ",", " ", 
    RowBox[{"given", " ", "probability", " ", 
     RowBox[{"p", ".", "\[IndentingNewLine]", "For"}], " ", "instance"}], ",",
     " ", 
    RowBox[{
     RowBox[{"f", "[", 
      RowBox[{"18", ",", " ", "2", ",", " ", "0.4"}], "]"}], " ", "will", " ",
      "calculate", " ", "18", "C2", "*", 
     RowBox[{
      RowBox[{"(", "0.4", ")"}], "^", "2"}], "*", 
     RowBox[{
      RowBox[{"(", "0.6", ")"}], "^", "16."}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"f", "[", 
     RowBox[{"n_", ",", " ", "i_", ",", " ", "p_"}], "]"}], " ", ":=", " ", 
    RowBox[{"Probability", "[", 
     RowBox[{
      RowBox[{"x", " ", "\[Equal]", " ", "i"}], ",", 
      RowBox[{"x", "\[Distributed]", 
       RowBox[{"BinomialDistribution", "[", 
        RowBox[{"n", ",", "p"}], "]"}]}]}], "]"}]}], ";"}]}]], "Input",
 CellChangeTimes->{{3.6105261337499933`*^9, 3.610526166400522*^9}, {
  3.610527391474411*^9, 3.6105274615324163`*^9}, {3.610530414332317*^9, 
  3.610530421635317*^9}, {3.610531099575317*^9, 3.610531243989317*^9}, {
  3.6105387577314773`*^9, 3.610538758798584*^9}, {3.6105388344491463`*^9, 
  3.6105389299119606`*^9}, {3.6105391097289324`*^9, 3.610539116631623*^9}, {
  3.610539254528226*^9, 3.6105392558513584`*^9}, {3.6105393548712597`*^9, 
  3.6105393772785*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "For", " ", "each", " ", "run", " ", "we", " ", "need", " ", "to", " ", 
     "make"}], ",", " ", 
    RowBox[{"so", " ", "each", " ", "line", " ", "of", " ", "input"}]}], " ", 
   "*)"}], "\[IndentingNewLine]", 
  RowBox[{"For", "[", 
   RowBox[{
    RowBox[{"r", " ", "=", " ", "0"}], ",", " ", 
    RowBox[{"r", " ", "<", " ", "numberOfRuns"}], ",", " ", 
    RowBox[{"r", "++"}], ",", "\[IndentingNewLine]", 
    RowBox[{"(*", 
     RowBox[{
      RowBox[{"We", " ", "set", " ", "the", " ", "probabilities"}], ",", " ", 
      RowBox[{"and", " ", "n", " ", 
       RowBox[{"value", ".", " ", "It"}], " ", "should", " ", "be", " ", 
       "noted", " ", "that", " ", "Mathematica", " ", "uses", " ", "strange", 
       " ", "un", " ", "java", " ", "like", " ", 
       RowBox[{"indexing", ".", "\[IndentingNewLine]", "So"}], " ", "we", " ",
        "start", " ", "at", " ", "1.", " ", "Like", " ", "normal", " ", 
       "people"}], ",", " ", "also", ",", " ", 
      RowBox[{"it", " ", "handles", " ", "whole", " ", "numbers", " ", 
       RowBox[{"fine", ".", " ", "But"}], " ", "I", " ", "have", " ", "to", 
       " ", "numberformat", " ", "the", " ", 
       RowBox[{"percentages", ".", "\[IndentingNewLine]", "Otherwise"}], " ", 
       "evil", " ", "things", " ", "occur"}], ",", " ", 
      RowBox[{
      "we", " ", "start", " ", "at", " ", "the", " ", "second", " ", "row"}], 
      ",", " ", 
      RowBox[{
       RowBox[{"hence", " ", "r"}], "+", "2"}]}], " ", "*)"}], 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"n", " ", "=", 
      RowBox[{"Part", "[", 
       RowBox[{"DataList", ",", 
        RowBox[{"r", "+", "2"}], ",", "1"}], "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"pTeamOne", " ", "=", " ", 
      RowBox[{"N", "[", 
       RowBox[{
        RowBox[{"Part", "[", 
         RowBox[{"DataList", ",", 
          RowBox[{"r", "+", "2"}], ",", "2"}], "]"}], "/", "100"}], "]"}]}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"pTeamTwo", " ", "=", " ", 
      RowBox[{"N", "[", 
       RowBox[{
        RowBox[{"Part", "[", 
         RowBox[{"DataList", ",", 
          RowBox[{"r", "+", "2"}], ",", "3"}], "]"}], "/", "100"}], "]"}]}], 
     ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{
       RowBox[{
       "Here", " ", "we", " ", "calculate", " ", "the", " ", "probability", 
        " ", "that", " ", "team", " ", "one", " ", "scores", " ", "a", " ", 
        "prime", " ", "number", " ", "of", " ", "goals"}], ",", " ", 
       RowBox[{
       "using", " ", "a", " ", "simple", "\[IndentingNewLine]", "for", " ", 
        "loop"}], ",", " ", 
       RowBox[{
        RowBox[{"so", " ", "for", " ", "0"}], " ", "\[Rule]", " ", "n"}], ",",
        " ", 
       RowBox[{"if", " ", "i", " ", "is", " ", "prime"}], ",", " ", 
       RowBox[{"check", " ", 
        RowBox[{"f", "[", 
         RowBox[{"n", ",", " ", "i", ",", " ", "p"}], "]"}]}], ",", " ", 
       RowBox[{
       "and", " ", "add", " ", "the", " ", "result", " ", "to", " ", "our", 
        " ", "running", " ", "tally"}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"runningProbabilityOne", " ", "=", " ", "0"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"i", " ", "=", " ", "0"}], ",", " ", 
       RowBox[{"i", " ", "<=", " ", "n"}], ",", " ", 
       RowBox[{"i", "++"}], ",", " ", 
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{"PrimeQ", "[", "i", "]"}], ",", " ", 
         RowBox[{"runningProbabilityOne", " ", "+=", " ", 
          RowBox[{"f", "[", 
           RowBox[{"n", ",", "i", ",", " ", "pTeamOne"}], "]"}]}]}], "]"}]}], 
      "]"}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"Ibid", " ", "for", " ", "team", " ", "2"}], " ", "*)"}], 
     "\[IndentingNewLine]", 
     RowBox[{"runningProbabilityTwo", "=", " ", "0"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"i", " ", "=", " ", "0"}], ",", " ", 
       RowBox[{"i", " ", "<=", " ", "n"}], ",", " ", 
       RowBox[{"i", "++"}], ",", " ", 
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{"PrimeQ", "[", "i", "]"}], ",", " ", 
         RowBox[{"runningProbabilityTwo", " ", "+=", " ", 
          RowBox[{"f", "[", 
           RowBox[{"n", ",", "i", ",", " ", "pTeamTwo"}], "]"}]}]}], "]"}]}], 
      "]"}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{
       RowBox[{
       "We", " ", "need", " ", "to", " ", "calculate", " ", "the", " ", 
        "likelihood", " ", "that", " ", "NEITHER", " ", "team", " ", "scores",
         " ", "a", " ", "prime", " ", "number", " ", "of", " ", 
        RowBox[{"goals", ".", " ", "So"}], " ", "to", " ", "do", " ", "this", 
        " ", "we", " ", "take", "\[IndentingNewLine]", "1"}], "-", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"P", 
          RowBox[{"(", "PrimeGoalsTeam1", ")"}]}], ")"}], "*", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"1", "-", 
           RowBox[{
            RowBox[{
             RowBox[{"(", 
              RowBox[{"P", 
               RowBox[{"(", "PrimeGoalsTeam2", ")"}]}], ")"}], ".", 
             "\[IndentingNewLine]", "Another"}], " ", "way", " ", "to", " ", 
            "say", " ", "this", " ", "is"}]}], ",", " ", 
          RowBox[{
           RowBox[{"we", " ", "take", " ", 
            RowBox[{"(", 
             RowBox[{"1", "-", "runningProbabilityOne"}], ")"}], " ", "*", 
            " ", 
            RowBox[{"(", 
             RowBox[{"1", " ", "-", " ", "runningProbabilityTwo"}], ")"}], 
            "\[IndentingNewLine]", "We", " ", "then", " ", "do", " ", "1"}], 
           "-", 
           RowBox[{
           "theAbove", " ", "to", " ", "find", " ", "the", " ", "probability",
             " ", "that", " ", "there", " ", "are", " ", "in", " ", "fact", 
            " ", "a", " ", "prime", " ", "number", " ", "of", " ", "goals", 
            " ", "scored", " ", "by", " ", "a", " ", 
            RowBox[{"team", "."}]}]}]}], "\[IndentingNewLine]"}]}]}], "*)"}], 
     "\[IndentingNewLine]", 
     RowBox[{"probabilityPrimeGoals", " ", "=", " ", 
      RowBox[{"1", "-", " ", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"1", "-", "runningProbabilityOne"}], ")"}], " ", "*", " ", 
        RowBox[{"(", 
         RowBox[{"1", " ", "-", " ", "runningProbabilityTwo"}], ")"}]}]}]}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{"Here", " ", "we", " ", "print", " ", "out", " ", "our", " ", 
       RowBox[{"results", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"Print", "[", 
      RowBox[{
      "\"\<Probability that at least one team scores a prime number of goals, \
to 4 dp: \>\"", ",", 
       RowBox[{"NumberForm", "[", 
        RowBox[{"probabilityPrimeGoals", ",", 
         RowBox[{"{", 
          RowBox[{"5", ",", "4"}], "}"}]}], "]"}], ",", 
       "\"\<, for case: \>\"", ",", "r"}], "]"}]}]}], " ", 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 CellChangeTimes->{{3.6105260463345137`*^9, 3.6105260499042273`*^9}, {
   3.610526089640173*^9, 3.61052609042233*^9}, {3.61052617539332*^9, 
   3.6105262720986576`*^9}, {3.6105264630775805`*^9, 
   3.6105265082991023`*^9}, {3.610526545848857*^9, 3.610526561088381*^9}, {
   3.610526602646536*^9, 3.610526608514123*^9}, {3.6105266395252237`*^9, 
   3.610526650251296*^9}, {3.6105267622444944`*^9, 3.610526775266796*^9}, {
   3.6105268166259317`*^9, 3.610526824400709*^9}, {3.610526868051074*^9, 
   3.610526890827351*^9}, {3.6105273357368374`*^9, 3.6105273475840225`*^9}, {
   3.610527488308093*^9, 3.610527503454608*^9}, {3.6105276341476755`*^9, 
   3.6105276378040414`*^9}, {3.6105277025705175`*^9, 
   3.6105277041546755`*^9}, {3.6105277614204016`*^9, 
   3.6105277943626957`*^9}, {3.610528002816539*^9, 3.610528012968554*^9}, {
   3.610530336689703*^9, 3.610530424846317*^9}, {3.610530738723317*^9, 
   3.6105307581833167`*^9}, {3.6105308083093166`*^9, 
   3.6105308243563166`*^9}, {3.6105312569953165`*^9, 
   3.6105313244223166`*^9}, {3.6105313718353167`*^9, 
   3.6105313827293167`*^9}, {3.6105333569953165`*^9, 3.610533511147317*^9}, {
   3.610533585767317*^9, 3.6105336464603167`*^9}, {3.610533696333317*^9, 
   3.610533696430317*^9}, {3.610533829343317*^9, 3.610533861371317*^9}, {
   3.6105339155523167`*^9, 3.6105339534083166`*^9}, {3.6105339880663166`*^9, 
   3.610534001339317*^9}, {3.6105340344603167`*^9, 3.610534219648317*^9}, {
   3.610534407263317*^9, 3.6105344241603165`*^9}, {3.610534503616317*^9, 
   3.610534536372317*^9}, {3.6105348915213165`*^9, 3.610534978864317*^9}, {
   3.6105350098843164`*^9, 3.6105350183023167`*^9}, {3.610535825200317*^9, 
   3.610535864530317*^9}, {3.6105383861619773`*^9, 3.6105383891448717`*^9}, {
   3.6105390217066317`*^9, 3.610539040807396*^9}, {3.610539103378298*^9, 
   3.6105391041873784`*^9}, {3.6105392874745207`*^9, 3.610539297840557*^9}, {
   3.6105393291746902`*^9, 3.6105393311728897`*^9}, 3.610539394561228*^9, {
   3.6105394648212533`*^9, 3.6105394706518364`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Probability that at least one team scores a prime number of \
goals, to 4 dp: \"\>", "\[InvisibleSpace]", 
   TagBox[
    InterpretationBox["\<\"1.0000\"\>",
     1,
     Editable->False],
    NumberForm[#, {5, 4}]& ], "\[InvisibleSpace]", "\<\", for case: \"\>", 
   "\[InvisibleSpace]", "0"}],
  SequenceForm[
  "Probability that at least one team scores a prime number of goals, to 4 \
dp: ", 
   NumberForm[1, {5, 4}], ", for case: ", 0],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.610533864673317*^9, {3.6105339196393166`*^9, 3.610534006950317*^9}, 
   3.6105341451583166`*^9, 3.6105341751773167`*^9, 3.6105342227623167`*^9, {
   3.610534283994317*^9, 3.610534301815317*^9}, 3.610534427872317*^9, {
   3.6105345255473166`*^9, 3.610534539431317*^9}, {3.610534896438317*^9, 
   3.6105349481503167`*^9}, 3.610535074359317*^9, 3.610535434300317*^9, {
   3.610535793530317*^9, 3.610535802963317*^9}, {3.610535870155317*^9, 
   3.610535886626317*^9}, 3.6105359982033167`*^9, 3.6105360349853168`*^9, 
   3.610538102913788*^9, 3.61053814957535*^9, {3.6105382590425262`*^9, 
   3.610538313309182*^9}, 3.6105383721047726`*^9, {3.6105384188287745`*^9, 
   3.610538451946707*^9}, 3.610538584180125*^9, {3.6105389198915596`*^9, 
   3.6105389806949916`*^9}, 3.6105390438975196`*^9, 3.6105392652352967`*^9, 
   3.610539301206894*^9, {3.610539335822355*^9, 3.6105393999637685`*^9}, 
   3.610539473981169*^9}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Probability that at least one team scores a prime number of \
goals, to 4 dp: \"\>", "\[InvisibleSpace]", 
   TagBox[
    InterpretationBox["\<\"0.5266\"\>",
     0.5265618908306355,
     AutoDelete->True],
    NumberForm[#, {5, 4}]& ], "\[InvisibleSpace]", "\<\", for case: \"\>", 
   "\[InvisibleSpace]", "1"}],
  SequenceForm[
  "Probability that at least one team scores a prime number of goals, to 4 \
dp: ", 
   NumberForm[0.5265618908306355, {5, 4}], ", for case: ", 1],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.610533864673317*^9, {3.6105339196393166`*^9, 3.610534006950317*^9}, 
   3.6105341451583166`*^9, 3.6105341751773167`*^9, 3.6105342227623167`*^9, {
   3.610534283994317*^9, 3.610534301815317*^9}, 3.610534427872317*^9, {
   3.6105345255473166`*^9, 3.610534539431317*^9}, {3.610534896438317*^9, 
   3.6105349481503167`*^9}, 3.610535074359317*^9, 3.610535434300317*^9, {
   3.610535793530317*^9, 3.610535802963317*^9}, {3.610535870155317*^9, 
   3.610535886626317*^9}, 3.6105359982033167`*^9, 3.6105360349853168`*^9, 
   3.610538102913788*^9, 3.61053814957535*^9, {3.6105382590425262`*^9, 
   3.610538313309182*^9}, 3.6105383721047726`*^9, {3.6105384188287745`*^9, 
   3.610538451946707*^9}, 3.610538584180125*^9, {3.6105389198915596`*^9, 
   3.6105389806949916`*^9}, 3.6105390438975196`*^9, 3.6105392652352967`*^9, 
   3.610539301206894*^9, {3.610539335822355*^9, 3.6105393999637685`*^9}, 
   3.6105394739931707`*^9}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1264, 665},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"8.0 for Microsoft Windows (64-bit) (November 7, 2010)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 5125, 104, 492, "Input"],
Cell[5685, 126, 1653, 35, 112, "Input"],
Cell[7341, 163, 860, 16, 31, "Input"],
Cell[8204, 181, 1948, 45, 92, "Input"],
Cell[CellGroupData[{
Cell[10177, 230, 9211, 192, 552, "Input"],
Cell[CellGroupData[{
Cell[19413, 426, 1457, 28, 23, "Print"],
Cell[20873, 456, 1494, 28, 23, "Print"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
