package assignment3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
 * @author Crijn van Vlokhoven
 */

public class FootballIsSimple {

	static int[] primes = { 2, 3, 5, 7, 11, 13, 17, 19 };
	
	final static String filename = "FootbalInput"; // TODO Change filename here

	public static void main(String[] args) throws FileNotFoundException {
		
		FileInputStream stream = new FileInputStream(filename);
		Scanner scan = new Scanner(stream);

		int cases = scan.nextInt();

		for (int i = 0; i < cases; i++) {
			int pointer = 0;
			double chance = 0.0;
			
			int n = scan.nextInt();
			int pA = scan.nextInt();
			int pB = scan.nextInt();
			
			double p1 = 0.0;
			double p2 = 0.0;
			
			for(int j=2; j<= n; j++){
				if (primes[pointer] == j) {
					p1 += nCr(n, primes[pointer], (pA / 100.0));
					p2 += nCr(n, primes[pointer], (pB / 100.0));
					pointer++;
				}
			}
			System.out.println(String.format("%.4g", p1 + p2 - (p1 * p2)));
		}
	}

	public static double nCr(int n, int r, double p) {
		if (n < r || p > 1.0)
			return -1;
		else {
			long fact = 1;
			for (int i = r + 1; i <= n; i++) {
				fact *= i;
			}
			long fact2 = 1;
			for (int i = 1; i <= (n - r); i++) {
				fact2 *= i;
			}
			double ncr = (fact / fact2) * 1.0;
			double chance1 = 1.0;
			for (int i = 0; i < r; i++) {
				chance1 *= p;
			}
			double chance2 = 1.0;
			for (int i = 0; i < (n - r); i++) {
				chance2 *= (1.0 - p);
			}
			return ncr * chance1 * chance2;
		}
	}
}
