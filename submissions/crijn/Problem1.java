package crijn;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector;

/**
 * 
 * 
 * @author Crijn van Vlokhoven
 */

public class Problem1 {

	final static String filename = "problem input/Bobby the poor librarian/test_input.txt"; // TODO Change filename here
	
	public static int[] search(String line) {
		Vector<String> searches = new Vector<String>();
		Scanner scan = new Scanner(line).useDelimiter("\\|");
		String text = "";

		while (scan.hasNext()) {
			String str = scan.next();
			if (scan.hasNext()) {
				searches.add(str);
			} else {
				text = str;
			}
		}

		int[] results = new int[searches.size()];

		for (int i = 0; i < text.length(); i++) {
			for (int j = 0; j < searches.size(); j++) {
				if (searches.get(j)
							.length() + i <= text.length()) {
					if (text.substring(i, i + searches.get(j)
														.length())
							.equals(searches.get(j))) {
						results[j] += 1;
					}
				}
			}
		}

		for (int i = 0; i < results.length; i++) {
			System.out.println(results[i]);
		}


		scan.close();
		return results;
	}

	public static void main(String[] args) throws FileNotFoundException {
		FileInputStream stream = new FileInputStream(filename);
		Scanner scan = new Scanner(stream);
		while(scan.hasNext()){
			search(scan.nextLine());
			if (scan.hasNext()) {
				System.out.println("");
			}
		}
		scan.close();
	}
}