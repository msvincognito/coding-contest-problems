package crijn;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
 * @author Crijn van Vlokhoven
 */

public class Problem2 {

	final static String filename = "problem input/BigFactorial/bigDataSetInput.txt"; // TODO Change filename here

	public static void main(String[] args) throws FileNotFoundException {

		FileInputStream stream = new FileInputStream(filename);
		Scanner scan = new Scanner(stream);

		int cases = scan.nextInt();

		for (int i = 0; i < cases; i++) {
			FatInteger fi = new FatInteger("1");
			fi.factorial(scan.nextInt());
			System.out.println(fi.toString() + "\n");
		}

		scan.close();
	}
}

class FatInteger {  // Because it's big and slow.

	private String value;

	public FatInteger(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void factorial(int number) {
		for (int i = 1; i <= number; i++) {
			multiply(i);
		}
	}

	public void multiply(int number){
		StringBuilder oldValue = new StringBuilder(this.value);

		FatInteger fint = new FatInteger("0");
		oldValue.reverse();

		for (int i = 0; i < oldValue.length(); i++) {
			StringBuilder str = new StringBuilder();
			int n = (oldValue.charAt(i) - 48) * number;
			for (int j = 0; j < i; j++) {
				str.append("" + 0);
			}
			while (n > 9) {
				str.append(n % 10);
				n = n / 10;
			}
			str.append(n);
			str.reverse();
			fint.add(str.toString());
		}
		value = fint.getValue();
	}

	public void add(String value) {
		StringBuilder oldValue = new StringBuilder(this.value);
		StringBuilder addValue = new StringBuilder(value);
		StringBuilder str = new StringBuilder();

		oldValue.reverse();
		addValue.reverse();

		int length = Math.max(oldValue.length(), addValue.length());

		while (oldValue.length() < length) {
			oldValue.append("0");
		}

		while (addValue.length() < length) {
			addValue.append("0");
		}

		for (int i = 0; i < length; i++) {
				if (str.length() > i) {
					int n = str.charAt(i) - 48;
					str.delete(i, i + 1);
					n += oldValue.charAt(i) + addValue.charAt(i) - 96;
					if (n > 9) {
						str.append(n - 10);
						str.append(1);
					} else {
						str.append(n);
					}
				} else {
					int n = oldValue.charAt(i) + addValue.charAt(i) - 96;
					if (n > 9) {
						str.append(n - 10);
						str.append(1);
					} else {
						str.append(n);
					}
				}
		}
		str.reverse();
		this.value = str.toString();
	}

	public String toString() {
		return value;
	}
}
