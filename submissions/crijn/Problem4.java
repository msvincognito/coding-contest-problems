package crijn;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
 * @author Crijn van Vlokhoven
 */

public class Problem4 {

	final static String filename = "../problem input/Match Model/sample input.txt"; // TODO Change filename here

	public static void main(String[] args) throws FileNotFoundException {

		FileInputStream stream = new FileInputStream(filename);
		Scanner scan = new Scanner(stream);

		int cases = scan.nextInt();

		for (int i = 0; i < cases; i++) {
			System.out.println(match(scan.nextLong()));
		}

		scan.close();
	}

	private static long match(long n) {

		long approx = 0;
		long matches = 0;
		int square = 1;
		long rest = n;

		for (int i = 1; i < n; i++) {
			if (approx <= n && Math.pow(i, 3) > n) {
				break;
			} else {
				approx = (int) Math.pow(i, 3);
				square = i;
			}
		}

		if (Math.pow(square + 1, 3) - n <= Math.pow(square + 1, 2)) {
			matches += calcCube(square + 1, square + 1, square);
			matches += calcLeftOver(n - ((square + 1) * (square + 1) * (square)));
		} else {
			rest = n - (long) Math.pow(square, 3);
			if (rest - (square * square) > 0) {
				matches += calcCube(square, square, square + 1);
				rest = rest - (square * square);
			} else {
				matches += calcCube(square, square, square);
			}

			matches += calcLeftOver(rest);
		}

		return matches;
	}

	private static long calcLeftOver(long n) {
		if (n == 0) {
			return 0;
		}
		long matches = 0;
		long square = (long) (Math.floor(Math.sqrt(n)));

		matches += calcSquare(square);

		long rest = n - (square * square);

		if (rest > 0)
			if (rest <= square) {
				matches += ((rest + 1) * 3 - 1);
			} else {
				matches += (((square + 1) * 3 - 1) + (((rest - square) + 1) * 3 - 1));
			}
		return matches;
	}

	private static long calcSquare(long square) {
		return (matchesInFace(square, square) + (square + 1) * (square + 1));
	}

	private static long calcCube(long w, long h, long l) {
		return ((matchesInFace(w, h) * (l + 1)) + (w + 1) * (h + 1) * l);
	}

	private static long matchesInFace(long width, long height) {
		return (width * (height + 1) + (width + 1) * height);
	}
}
