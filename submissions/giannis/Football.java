package incognitoContest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Football {

	private int intervals;
	private double p1;
	private double p2;

	public Football(int intervals, double p1, double p2) {
		this.intervals = intervals;
		this.p1 = p1;
		this.p2 = p2;
	}

	public double computeProbability() {
		List<Integer> primes = primeNumbersUpTo(intervals);
		double firstTeamProb = 0;
		double secondTeamProb = 0;
		for (int prime : primes) {
			firstTeamProb += binomial(intervals, prime) * Math.pow(p1, prime) * Math.pow(1 - p1, intervals - prime);
			secondTeamProb += binomial(intervals, prime) * Math.pow(p2, prime) * Math.pow(1 - p2, intervals - prime);
		}
		return 1 - (1 - firstTeamProb) * (1 - secondTeamProb);
	}

	public static double binomial(final int N, final int K) {
		// assert N >= K;
		assert N >= 0 && K >= 0;
		double ret = 1;
		for (int k = 0; k < K; k++) {
			ret = ret * (N - k) / (k + 1);
		}
		return ret;
	}

	public static List<Integer> primeNumbersUpTo(int n) {
		List<Integer> primes = new ArrayList<Integer>();
		for (int i = 2; i <= n; i++) {
			boolean isPrime = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				primes.add(i);
			}
		}
		return primes;
	}

	public static void main(String[] args) {
		// Filename, change this
		String fileName = "football.txt";

		try {
			Scanner scanner = new Scanner(new File(fileName));
			int numTestCases = scanner.nextInt();
			for (int i = 0; i < numTestCases; i++) {
				int intervals = scanner.nextInt();
				double p1 = scanner.nextDouble() / 100.0;
				double p2 = scanner.nextDouble() / 100.0;
				Football problem = new Football(intervals, p1, p2);
				double prob = problem.computeProbability();
				System.out.println(String.format("%.4g%n", prob));
			}
			scanner.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
