package giannis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Problem1 {

	private String text;
	private List<String> strings;
	private int[] lastMatch;
	private int[] counts;


	public Problem1(String fileName) {
		// Scanner scanner = new Scanner("obby is a nice per|Bobby|lib|dog|Bobby is a nice person, he is a librarian.");
		Scanner scanner;
		try {
			scanner = new Scanner(new File(fileName));
			while (scanner.hasNextLine()) {
				readLine(scanner);
				solve();
			}
			scanner.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void readLine(Scanner scanner) {
		String line = scanner.nextLine();
		String[] split = line.split("\\|");
		text = split[split.length - 1];
		strings = new ArrayList<String>();
		lastMatch = new int[split.length - 1];
		counts = new int[split.length - 1];
		for (int i = 0; i < split.length - 1; i++) {
			strings.add(split[i]);
		}
	}

	public void solve() {
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			for (int j = 0; j < strings.size(); j++) {
				if (strings.get(j).charAt(lastMatch[j]) == c) {
					lastMatch[j]++;
					if (lastMatch[j] == strings.get(j).length()) {
						counts[j]++;
						lastMatch[j] = 0;
					}
				}
				else {
					lastMatch[j] = 0;
				}
			}
		}
		for (int count : counts) {
			System.out.println(count);
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Problem1 problem = new Problem1("problem input/Bobby the poor librarian/test_input.txt");
	}
}
