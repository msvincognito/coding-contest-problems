package giannis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Problem2 {


	public static void main(String[] args) {
		// Filename, change this
		String fileName = "problem input/BigFactorial/bigDataSetInput.txt";

		try {
			Scanner scanner = new Scanner(new File(fileName));
			int numTestCases = scanner.nextInt();
			for (int i = 0; i < numTestCases; i++) {
				int n = scanner.nextInt();
				String factorial = computeFactorial(n);
				System.out.println(factorial);
			}
			scanner.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	public static String computeFactorial(int number) {
		int maxDigits = 1000;
		int base = 10;
		int[] digits = new int[maxDigits + 1];
		int carry, d;
		int last, i;
		digits[1] = 1;
		last = 1;
		for (int n = 1; n <= number; n++) {
			carry = 0;
			for (i = 1; i <= last; i++) {
				d = digits[i] * n + carry;
				digits[i] = d % base;
				carry = d / base;
			}
			while (carry > 0) {
				if (last >= maxDigits) {
					throw new RuntimeException("Overflow");
				}
				last = last + 1;
				digits[last] = carry % base;
				carry = carry / base;
			}
		}
		return toString(digits, last);
	}

	public static String toString(int[] reverseDigits, int lastIndex) {
		String result = "";
		for (int i = lastIndex; i > 0; i--) {
			result += reverseDigits[i];
		}
		return result;
	}

	// Wikipedia pseudocode

	// Constant Limit = 1000; %Sufficient digits.
	// Constant Base = 10; %The base of the simulated arithmetic.
	// Constant FactorialLimit = 365; %Target number to solve, 365!
	// Array digit[1:Limit] of integer; %The big number.
	// Integer carry,d; %Assistants during multiplication.
	// Integer last,i; %Indices to the big number's digits.
	// Array text[1:Limit] of character;%Scratchpad for the output.
	// Constant tdigit[0:9] of character = ["0","1","2","3","4","5","6","7","8","9"];
	// BEGIN
	// digit:=0; %Clear the whole array.
	// digit[1]:=1; %The big number starts with 1,
	// last:=1; %Its highest-order digit is number 1.
	// for n:=1 to FactorialLimit do %Step through producing 1!, 2!, 3!, 4!, etc.
	// carry:=0; %Start a multiply by n.
	// for i:=1 to last do %Step along every digit.
	// d:=digit[i]*n + carry; %The classic multiply.
	// digit[i]:=d mod Base; %The low-order digit of the result.
	// carry:=d div Base; %The carry to the next digit.
	// next i;
	// while carry > 0 %Store the carry in the big number.
	// if last >= Limit then croak("Overflow!"); %If possible!
	// last:=last + 1; %One more digit.
	// digit[last]:=carry mod Base; %Placed.
	// carry:=carry div Base; %The carry reduced.
	// Wend %With n > Base, maybe > 1 digit extra.
	// text:=" "; %Now prepare the output.
	// for i:=1 to last do %Translate from binary to text.
	// text[Limit - i + 1]:=tdigit[digit[i]]; %Reversing the order.
	// next i; %Arabic numerals put the low order last.
	// Print text," = ",n,"!"; %Print the result!
	// next n; %On to the next factorial up.
	// END;
}
