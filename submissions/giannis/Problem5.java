package giannis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Problem5 {

	static boolean[] isPrime;
	static List<Integer> primes;
	static List<Integer> UDNs;

	public static int findLargestUDNfactorization() {
		// 2437890 = 1 * 2 * 3 * 5 * 7 * 13 * 19 * 47
		// 1345890 = 1 * 2 * 3 * 5 * 7 * 13 * 17 * 29
		// 48362790 = 1 * 2 * 3 * 5 * 7 * 17 * 19 * 23 * 31
		int max = 50 * 1000 * 1000;
		primes = findPrimesUpTo(max);
		UDNs = findUDNsUpTo(max);
		int[] numFactors = new int[100];
		int[] numFactorsUDN = new int[100];
		List<SolutionPair> potentials = new ArrayList<SolutionPair>();
		numFactors[0] = 1;
		SolutionPair best = new SolutionPair(12375, Arrays.asList(5, 15, 165));
		System.out.println("finished primes");
		System.out.println(UDNs.size());
		for (int i = 2; i < UDNs.size(); i++) {
			// if (i % (UDNs.size() / 10) == 0) {
			// System.out.println(i + " n " + UDNs.get(i) + " " + best);
			// }
			int udn = UDNs.get(i);
			List<Integer> primeFactors = findPrimeFactors(udn);
			numFactors[primeFactors.size()]++;
			numFactorsUDN[primeFactors.size()]++;
			if (primeFactors.size() <= best.getFactorization().size()) {
				continue;
			}
			List<Integer> udnFactors = new ArrayList<Integer>();
			for (int p : primeFactors) {
				if (isUDN(p) && !udnFactors.contains(p)) {
					udnFactors.add(p);
				}
			}
			SolutionPair pair = new SolutionPair(udn, udnFactors);
			// if (udnFactors.size() >= 7) {
			// enhanceSolution(pair);
			// }
			if (udnFactors.size() >= 8) {
				// System.out.println(udn + " " + udnFactors);
				potentials.add(pair);
			}
			if (udnFactors.size() > best.getFactorization().size()) {
				best = pair;
			}
		}
		// System.out.println(Arrays.toString(numFactors));
		// System.out.println(Arrays.toString(numFactorsUDN));
		// System.out.println(potentials.size() + " " + potentials);
		System.out.println("best " + best.getNumber() + " factorization " + best.getFactorization() + " "
							+ best.getFactorization().size() + " factors");
		return 0;
	}

	public static void enhanceSolution(SolutionPair solution) {
		@SuppressWarnings("unchecked")
		List<SolutionPair> improvements = new ArrayList<SolutionPair>();
		improvements.add(solution);
		for (int i = 2; i < UDNs.size(); i++) {
			for (int j = improvements.size() - 1; j < improvements.size(); j++) {
				SolutionPair pair = improvements.get(j);
				int udn = UDNs.get(i);
				long newNumber = pair.getNumber() * udn;
				if (newNumber < Integer.MAX_VALUE && isUDN((int) newNumber) && !pair.getFactorization().contains(udn)) {
					ArrayList<Integer> newFactorization = new ArrayList<>(pair.getFactorization());
					newFactorization.add(udn);
					improvements.add(new SolutionPair((int) newNumber, newFactorization));
					System.out.println("improvement " + newFactorization.size());
				}
			}
		}
		if (improvements.size() > 1)
			System.out.println(improvements);
	}

	public static List<Integer> findPrimeFactors(int n) {
		List<Integer> factors = new ArrayList<Integer>();
		while (n > 1) {
			for (int p : primes) {
				if (isPrime[n]) {
					factors.add(n);
					n = n / n;
				}
				if (n == 1) {
					break;
				}
				if (n % p == 0) {
					do {
						factors.add(p);
						n = n / p;
					} while (n % p == 0);
				}
			}
		}
		return factors;
	}

	public static List<Integer> findUDNsUpTo(int n) {
		List<Integer> UDNs = new ArrayList<Integer>();
		for (int i = 1; i < n; i++) {
			if (isUDN(i)) {
				UDNs.add(i);
			}
		}
		return UDNs;
	}

	public static List<Integer> findPrimesUpTo(int n) {
		isPrime = new boolean[n + 1];
		List<Integer> primes = new ArrayList<Integer>();
		Arrays.fill(isPrime, true);
		isPrime[0] = isPrime[1] = false;
		for (int i = 4; i <= n; i += 2) {
			isPrime[i] = false;
		}
		for (int i = 3; i <= n; i += 2) {
			if (isPrime[i]) {
				for (int j = i * 2; j <= n; j += i) {
					isPrime[j] = false;
				}
			}
		}
		for (int i = 0; i < isPrime.length; i++) {
			if (isPrime[i]) {
				primes.add(i);
			}
		}
		return primes;
	}

	private static boolean isUDN(int n) {
		LinkedList<Integer> digits = getDigits(n);
		Collections.sort(digits);
		if (digits.size() > 9) {
			return false;
		}
		for (int i = 0; i < digits.size() - 1; i++) {
			if (digits.get(i) == digits.get(i + 1)) {
				return false;
			}
		}
		return true;
	}
	


	public static LinkedList<Integer> getDigits(int number) {
		LinkedList<Integer> stack = new LinkedList<Integer>();
		while (number > 0) {
			stack.push(number % 10);
			number = number / 10;
		}
		return stack;
	}

	public static void testUDN() {
		assert !isUDN(313);
		assert isUDN(123456789);
		assert !isUDN(1234561);
	}

	public static void main(String[] args) {
		findLargestUDNfactorization();
	}
}

class SolutionPair {

	int number;
	List<Integer> factorization;

	public SolutionPair(int number, List<Integer> factorization) {
		this.number = number;
		this.factorization = factorization;
	}

	public int getNumber() {
		return number;
	}

	public List<Integer> getFactorization() {
		return factorization;
	}

	@Override
	public String toString() {
		return "SolutionPair [number=" + number + ", factorization=" + factorization + "]";
	}

}
