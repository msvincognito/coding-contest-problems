package gijsjan;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

/**
 * https://msvincognito.nl/coding-contest/
 * Description problem:
 * 
 * <pre>
 * Problem 1: Bobby the librarian
 * 
 * 		Assume Bobby gives you a large text, together with multiple search strings.
 * 		Write an algorithm that outputs the number of occurrences of each string, one per line, with the restriction that you can only loop through the text ONCE.
 * 		
 * 		So if string 1 occurs 2 times, string 2 occurs 4 times, string 3 7 times and string 4 0 times, the output would be:
 * 		2
 * 		4
 * 		7
 * 		0
 * 		
 * 		Note you cannot assume that a string contains no spaces, actually a search string can contain ANY characters, except ‘|’. So a valid search string could be ‘obby is a nice pers’.
 * 		Also, the books can have any length, but you can expect them to be 1 or 2 paragraphs.
 * 		Finally, search strings can overlap, so search strings ‘Bobby’ and ‘bby’ in the string ‘Bobby is a librarian.’ both occur once.
 * 		
 * 		The input will have the following format (i.e. each test case is formatted in one line as this, there can be multiple lines if there are multiple test cases):
 * 		<search_string_1>|<search_string_2>|…|<search_string_n>|<text>
 * 		For example
 * 		obby is a nice per|Bobby|lib|dog|Bobby is a nice person, he is a librarian.
 * </pre>
 * 
 * @author GJ Roelofs <info@codepoke.net>
 *
 */
public class Problem1Submission1 {

	// Regex to separate out the text from the qualifiers
	public static final String REGEX_INPUT_SEPARATOR = "^(.*)\\|([^|]*)$";
	public static final String KEYWORD_SEPARATOR = "\\|";

	/**
	 * Solves the above described solution for a single question line
	 * 
	 * @param input
	 *            String which separates with KEYWORD_SEPARATOR, has at least
	 * @return A String describing on each line the number of occurences for each keyword
	 */
	public static String solve(String input) {

		Matcher matcher = Pattern.compile(REGEX_INPUT_SEPARATOR)
									.matcher(input);

		if (!matcher.find()) {
			System.err.println("Input maldefined, can't separate, go away");
			System.exit(-42);
		}

		// Split the keywords and extract the text from the Matcher
		String[] keywords = matcher.group(1)
									.split(KEYWORD_SEPARATOR);

		System.out.print("Searching for: ");
		for (String keyWord : keywords)
			System.out.print(keyWord + ", ");

		String text = matcher.group(2);

		System.out.println("\n\nIn: " + text + "\n");

		// Instantiate required bookkeepers
		int[] counter = new int[keywords.length];		// How many found of String X
		int[] keywordIndex = new int[keywords.length];	// Which index we are in the String

		/**
		 * Search!
		 * Because using escaped keywords as Regex would probably be cheating.
		 */
		for (int i = 0; i < text.length(); i++) {

			char toCheck = text.charAt(i);

			for (int j = 0; j < keywords.length; j++) {
				String keyWord = keywords[j];
				char toMatch = keyWord.charAt(keywordIndex[j]);

				if (toCheck == toMatch) {
					// If equal, log and increase the index counter for that String
					keywordIndex[j]++;
					if (keywordIndex[j] == keyWord.length()) {
						counter[j]++;
						keywordIndex[j] = 0;
					}
				} else {
					// If not, reset the index counter
					keywordIndex[j] = 0;
				}
			}
		}

		/**
		 * Output!
		 */

		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < counter.length; i++) {
			buf.append(counter[i]);
			buf.append("\n");
		}

		System.out.println("Solution found: \n" + buf.toString());

		return buf.toString();
	}

	public static void main(String[] args) throws IOException {

		/**
		 * ALTER THIS STRING FOR THE INPUT FILE
		 */
		String input = "problem input/Bobby the poor librarian/test_input.txt";
		String output = "problem1submission1_output.txt";

		// Read the file
		String contents = FileUtils.readFileToString(new File(input));
		String[] questions = contents.split("\n");

		StringBuffer completeAnswer = new StringBuffer();

		// Solve all the Questions
		for (String question : questions) {
			completeAnswer.append(solve(question));
			completeAnswer.append("\n");
		}

		try {
			File outputFile = new File(output);

			// Create directories if they don't exist yet
			outputFile.getParentFile()
						.mkdirs();

			PrintWriter out = new PrintWriter(outputFile);
			out.print(completeAnswer.toString());
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
