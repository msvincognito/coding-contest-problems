package melanie;

import java.util.Observable;

/**
 * 
 * @author Melanie Thissen
 *
 */
public class Book extends Observable {

	/**
	 * The provided text.
	 */
	private String text;
	/**
	 * Current index.
	 */
	private int index = 0;

	public Book(String text) {
		this.text = text;
	}

	/**
	 * Returns the next char and notifies all observers of the new character.
	 * 
	 * @return next character in the text
	 */
	public char readNextChar() {
		char nextChar = text.charAt(index);
		setChanged();
		notifyObservers(nextChar);
		++index;
		return nextChar;
	}

	public boolean hasNextChar() {
		return index < text.length();
	}
}
