package melanie;

import java.util.Arrays;

/**
 * 
 * @author Melanie Thissen
 *
 */
public class Edge {

	private int[] start;
	private int[] end;

	public Edge(int[] start, int[] end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o == this)
			return true;
		if (!o.getClass().equals(getClass()))
			return false;

		Edge other = (Edge) o;
		if (Arrays.equals(start, other.start) && Arrays.equals(end, other.end)) {
			return true;
		}
		if (Arrays.equals(start, other.end) && Arrays.equals(end, other.start)) {
			return true;
		}
		return false;
	}

	public String toString() {
		return "|" + Arrays.toString(start) + "-->" + Arrays.toString(end)
				+ "|";
	}

}
