package melanie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Melanie Thissen
 *
 */
public class Field3D {

	private boolean[][][] field;
	private int shift;
	private int numberOfCubes;

	public enum Axis {
		X, Y, Z
	}

	public Field3D(int dimension) {
		field = new boolean[dimension][dimension][dimension];
		this.shift = dimension / 2;
		this.numberOfCubes = 0;
	}

	public void addCube(int x, int y, int z) {
		x += shift;
		y += shift;
		z += shift;
		field[x][y][z] = true;
		++numberOfCubes;
	}

	public void removeCube(int x, int y, int z) {
		x += shift;
		y += shift;
		z += shift;
		field[x][y][z] = false;
		--numberOfCubes;
	}

	public boolean containsCube(int x, int y, int z) {
		x += shift;
		y += shift;
		z += shift;
		return field[x][y][z];
	}

	public boolean isPlaceForNewCube(int x, int y, int z) {
		x += shift;
		y += shift;
		z += shift;
		if (field[x][y][z]) {
			return false;
		} else {
			// check neighbours
			if ((x + 1 < field.length) && field[x + 1][y][z]) {
				return true;
			} else if ((y + 1 < field.length) && field[x][y + 1][z]) {
				return true;
			} else if ((z + 1 < field.length) && field[x][y][z + 1]) {
				return true;
			} else if ((x - 1 >= 0) && field[x - 1][y][z]) {
				return true;
			} else if ((y - 1 >= 0) && field[x][y - 1][z]) {
				return true;
			} else if ((z - 1 >= 0) && field[x][y][z - 1]) {
				return true;
			}
		}
		return false;
	}

	public Field3D turn(Axis a) {
		Field3D turned = new Field3D(field.length);
		for (int i = -shift; i <= shift; ++i) {
			for (int j = -shift; j <= shift; ++j) {
				for (int k = -shift; k <= shift; ++k) {
					if (containsCube(i, j, k)) {
						if (a == Axis.X) {
							turned.addCube(i, -k, j);
						} else if (a == Axis.Y) {
							turned.addCube(k, j, -i);
						} else if (a == Axis.Z) {
							turned.addCube(-j, i, k);
						}
					}
				}
			}
		}
		return turned;
	}

	public Field3D mirror(Axis a) {
		Field3D turned = new Field3D(field.length);
		for (int i = -shift; i <= shift; ++i) {
			for (int j = -shift; j <= shift; ++j) {
				for (int k = -shift; k <= shift; ++k) {
					if (containsCube(i, j, k)) {
						if (a == Axis.X) {
							turned.addCube(i, -j, -k);
						} else if (a == Axis.Y) {
							turned.addCube(-i, j, -k);
						} else if (a == Axis.Z) {
							turned.addCube(-i, -j, k);
						}
					}
				}
			}
		}
		return turned;
	}

	public int countEdges3() {
		List<Edge> allEdges = new ArrayList<Edge>();
		for (int i = 0; i < field.length; ++i) {
			for (int j = 0; j < field.length; ++j) {
				for (int k = 0; k < field.length; ++k) {
					if (field[i][j][k]) {
						List<Edge> edges = createAllEdges(i, j, k);
						for (Edge edge : edges) {
							if (!allEdges.contains(edge)) {
								allEdges.add(edge);
							}
						}
					}
				}
			}
		}
		return allEdges.size();
	}

	private List<Edge> createAllEdges(int i, int j, int k) {
		List<Edge> edges = new ArrayList<Edge>();
		Edge edge = new Edge(new int[] { i, j, k }, new int[] { i + 1, j, k });
		edges.add(edge);
		edge = new Edge(new int[] { i, j, k }, new int[] { i, j + 1, k });
		edges.add(edge);
		edge = new Edge(new int[] { i, j, k }, new int[] { i, j, k + 1 });
		edges.add(edge);
		edge = new Edge(new int[] { i + 1, j, k },
				new int[] { i + 1, j + 1, k });
		edges.add(edge);
		edge = new Edge(new int[] { i + 1, j, k },
				new int[] { i + 1, j, k + 1 });
		edges.add(edge);
		edge = new Edge(new int[] { i, j + 1, k },
				new int[] { i + 1, j + 1, k });
		edges.add(edge);
		edge = new Edge(new int[] { i, j + 1, k },
				new int[] { i, j + 1, k + 1 });
		edges.add(edge);
		edge = new Edge(new int[] { i, j, k + 1 },
				new int[] { i + 1, j, k + 1 });
		edges.add(edge);
		edge = new Edge(new int[] { i, j, k + 1 },
				new int[] { i, j + 1, k + 1 });
		edges.add(edge);
		edge = new Edge(new int[] { i + 1, j + 1, k }, new int[] { i + 1,
				j + 1, k + 1 });
		edges.add(edge);
		edge = new Edge(new int[] { i + 1, j, k + 1 }, new int[] { i + 1,
				j + 1, k + 1 });
		edges.add(edge);
		edge = new Edge(new int[] { i, j + 1, k + 1 }, new int[] { i + 1,
				j + 1, k + 1 });
		edges.add(edge);
		return edges;
	}

	public int getDimension() {
		return field.length;
	}

	public boolean[][][] getField() {
		return field;
	}

	public int getNumberOfCubes() {
		return numberOfCubes;
	}

	public int getShift() {
		return shift;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o == this)
			return true;
		if (!o.getClass().equals(getClass()))
			return false;

		Field3D other = (Field3D) o;
		return Arrays.deepEquals(field, other.field);
	}

	public Field3D clone() {
		Field3D clone = new Field3D(field.length);
		clone.numberOfCubes = numberOfCubes;
		for (int i = 0; i < field.length; ++i) {
			for (int j = 0; j < field.length; ++j) {
				for (int k = 0; k < field.length; ++k) {
					if (field[i][j][k]) {
						clone.field[i][j][k] = true;
					}
				}
			}
		}
		return clone;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < field.length; ++i) {
			sb.append("*******- " + i + " -*******"
					+ System.getProperty("line.separator")
					+ System.getProperty("line.separator"));
			for (int j = 0; j < field.length; ++j) {
				sb.append("[");
				for (int k = 0; k < field.length; ++k) {
					if (field[k][j][i]) {
						sb.append("O, ");
					} else {
						sb.append("X, ");
					}
					if (k == field.length - 1) {
						sb.setLength(sb.length() - 2);
						sb.append("]");
					}
				}
				sb.append(System.getProperty("line.separator"));
			}
			sb.append(System.getProperty("line.separator"));
		}
		return sb.toString();
	}

}
