package melanie;

/**
 * 
 * @author Melanie Thissen
 *
 */
public class FootBallNode {

	private double probability1;
	private double probability2;
	private int level;
	private int goal1;
	private int goal2;

	public FootBallNode(double prob1, double prob2, int level, int g1, int g2) {
		this.probability1 = prob1;
		this.probability2 = prob2;
		this.level = level;
		this.goal1 = g1;
		this.goal2 = g2;
	}

	public double getProbability1() {
		return probability1;
	}

	public double getProbability2() {
		return probability2;
	}

	public int getLevel() {
		return level;
	}

	public int getGoal1() {
		return goal1;
	}

	public int getGoal2() {
		return goal2;
	}
	
	

}
