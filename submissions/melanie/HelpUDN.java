package melanie;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
 * @author Melanie Thissen
 * 
 */
public class HelpUDN {

	private static String maxUdn;

	public static void main(String[] args) {
		readFile("src/task5/solution.txt");
		System.out.println(maxUdn);
	}

	private static void readFile(String filename) {
		File file = new File(filename);
		Scanner sc = null;
		int max = 0;
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				int first = line.indexOf(":");
				String key = line.substring(0, first);
				String rest = line.substring(first + 3, line.length() - 1);
				// System.out.println(key);
				// System.out.println(rest);
				String[] split1 = rest.split("\\[|\\]");
				String[] split2 = split1[1].split(",");
				if (split2.length >= max) {
					max = split2.length;
					maxUdn = key;
					System.out
							.println(key + ", " + split2.length + ", " + rest);
				}

			}
		} catch (FileNotFoundException e) {
			System.out.println("Error while reading file");
			e.printStackTrace();
		}
		sc.close();
	}

}
