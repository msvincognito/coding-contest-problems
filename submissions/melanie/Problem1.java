package melanie;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author Melanie Thissen
 * 
 */
public class Problem1 {

	/**
	 * Contains the number of occurrences for each search string.
	 */
	private static HashMap<String, Integer> searchStringCount = new HashMap<String, Integer>();
	/**
	 * Contains all search strings in the original order (for output).
	 */
	private static List<String> searchStrings = new ArrayList<String>();
	/**
	 * Contains the text that is to be scanned for search strings.
	 */
	private static String text = "";
	/**
	 * Contains all search strings that are possible matches at the moment.
	 */
	private static List<SearchString> currentlySearched = new ArrayList<SearchString>();

	public static void main(String[] args) {
		// if (args != null && args.length <= 2 && args[1] != null
		// && args[1].length() > 0) {
		// readFile(args[1]);
		readFile("problem input/Bobby the poor librarian/test_input.txt");
		// writeOutput("src/task1/Bobby.txt");
		// writeOutput(args[1]);
		// } else {
		// System.out.println("No input file provided.");
		// }
	}

	/**
	 * Splits the line at the specified parameter, saves all search strings and
	 * the text.
	 * 
	 * @param line
	 */
	private static void parseLine(String line) {
		String[] split = line.split("\\|");
		for (int i = 0; i < split.length - 1; ++i) {
			searchStringCount.put(split[i], 0);
			searchStrings.add(split[i]);
		}
		text = split[split.length - 1];
	}

	/**
	 * Reads the first line of the input file that contains the search strings
	 * and the text.
	 * 
	 * @param filename
	 *            name of input file
	 */
	private static void readFile(String filename) {
		File file = new File(filename);
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				searchStringCount = new HashMap<String, Integer>();
				searchStrings = new ArrayList<String>();
				currentlySearched = new ArrayList<SearchString>();
				text = "";
				parseLine(sc.nextLine());
				scanText();
				writeOutput("file");
				System.out.println();
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error while reading file");
			e.printStackTrace();
		}
		sc.close();
	}

	/**
	 * Scans the text. If a character matches the first character of a search
	 * string, a new observer is created. Afterwards the list of current
	 * observers is checked to remove not matching ones and finished ones.
	 */
	private static void scanText() {
		Book book = new Book(text);
		while (book.hasNextChar()) {
			char nextChar = book.readNextChar();
			for (String search : searchStrings) {
				if (nextChar == search.charAt(0)) {
					SearchString newSearch = new SearchString(search);
					currentlySearched.add(newSearch);
					book.addObserver(newSearch);
				}
			}
			for (int i = 0; i < currentlySearched.size(); ++i) {
				if (!currentlySearched.get(i).isCanFit()) {
					book.deleteObserver(currentlySearched.remove(i));
					--i;
				} else if (currentlySearched.get(i).isFinished()) {
					searchStringCount.put(currentlySearched.get(i)
							.getSearchString(),
							searchStringCount.get(currentlySearched.get(i)
									.getSearchString()) + 1);
					book.deleteObserver(currentlySearched.remove(i));
					--i;
				}
			}
		}
	}

	/**
	 * Writes output to command line.
	 * 
	 * @param filename
	 *            name of input file
	 */
	private static void writeOutput(String filename) {
		// File file = new File(filename + ".out");
		// FileWriter fw = null;
		// BufferedWriter bw = null;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < searchStrings.size(); ++i) {
			sb.append(searchStringCount.get(searchStrings.get(i))
					+ System.getProperty("line.separator"));
			System.out.println(searchStringCount.get(searchStrings.get(i)));
		}
		// try {
		// fw = new FileWriter(file);
		// bw = new BufferedWriter(fw);
		// bw.write(sb.toString());
		// bw.close();
		// } catch (IOException e) {
		// System.out.println("Error while writing file.");
		// e.printStackTrace();
		// }
	}
}
