package melanie;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
 * @author Melanie Thissen
 * 
 */
public class Problem2 {

	/**
	 * Contains list of all numbers given as input.
	 */
	private static String[] allNumbers;
	/**
	 * Contains all computed factorials.
	 */
	private static String[] allFactorials;

	public static void main(String[] args) {
		// if (args != null && args.length <= 2 && args[1] != null
		// && args[1].length() > 0) {
		// readFile(args[1]);
		readFile("problem input/BigFactorial/bigDataSetInput.txt");
		computeFactorials();
		writeOutput("submissions/melanie/problem2_big.txt");
		// writeOutput(args[1]);
		// } else {
		// System.out.println("No input file provided.");
		// }
	}

	/**
	 * Handles special cases and calls method to compute regular cases.
	 */
	private static void computeFactorials() {
		for (int i = 0; i < allNumbers.length; ++i) {
			if (allNumbers[i].equals("2")) {
				allFactorials[i] = "2";
			} else if (allNumbers[i].equals("1") || allNumbers[i].equals("0")) {
				allFactorials[i] = "1";
			} else {
				allFactorials[i] = doTheMathStuff(allNumbers[i]);
			}
		}
	}

	/**
	 * Creates a list of all numbers from 2 to (number-1) and calls a
	 * multiplication method.
	 * 
	 * @param number
	 *            this factorial is to be computed
	 * @return factorial
	 */
	private static String doTheMathStuff(String number) {
		String[] multi = new String[Integer.parseInt(number) - 2];
		for (int i = 0; i < multi.length; ++i) {
			multi[i] = (i + 2) + "";
		}
		String mult = number;
		for (String m : multi) {
			mult = multiply(mult, m);
		}
		return mult;
	}

	/**
	 * Multiplies the two given numbers.
	 * 
	 * @param num1
	 *            longer number
	 * @param num2
	 *            shorter number
	 * @return result of multiplication
	 */
	private static String multiply(String num1, String num2) {
		StringBuilder sb = new StringBuilder();
		// save the single multiplications
		int[][] interResults = new int[num2.length()][num1.length()
				+ num2.length()];
		int index2 = num2.length() - 1;
		int carry = 0;
		for (int row = 0; row < num2.length(); ++row) {
			int index1 = num1.length() - 1;
			for (int column = 0 + row; column < interResults[row].length; ++column) {
				if (index1 >= 0) {
					int result = (num1.charAt(index1) - '0')
							* (num2.charAt(index2) - '0');
					interResults[row][column] = (carry + result) % 10;
					carry = (carry + result) / 10;
				} else if (index1 == -1) {
					interResults[row][column] = carry;
				}
				--index1;
			}
			carry = 0;
			--index2;
		}
		// add the interim results
		carry = 0;
		for (int column = 0; column < interResults[0].length; ++column) {
			int sum = 0;
			for (int row = 0; row < num2.length(); ++row) {
				sum += interResults[row][column];
			}
			sb.append(((carry + sum) % 10));
			carry = (carry + sum) / 10;
		}
		for (int i = sb.length() - 1; i >= 0; --i) {
			if (sb.charAt(i) != '0') {
				break;
			}
			sb.deleteCharAt(i);
		}
		return sb.reverse().toString().trim();
	}

	/**
	 * Reads the amount of test cases from the first line and reads the numbers
	 * for that their factorial is to be computed.
	 * 
	 * @param filename
	 *            name of input file
	 */
	@SuppressWarnings("resource")
	private static void readFile(String filename) {
		File file = new File(filename);
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			// read first line
			int numberOfCases = 0;
			if (sc.hasNextLine()) {
				String line = sc.nextLine().trim();
				if (line.matches("[0-9]+")) {
					numberOfCases = Integer.parseInt(line);
					allNumbers = new String[numberOfCases];
					allFactorials = new String[numberOfCases];
				} else {
					throw new RuntimeException("Error in first line");
				}
			} else {
				throw new RuntimeException("File is empty");
			}
			// read numbers
			for (int i = 0; i < numberOfCases; ++i) {
				if (sc.hasNextLine()) {
					allNumbers[i] = sc.nextLine().trim();
				} else {
					throw new RuntimeException("File contains less lines than "
							+ numberOfCases);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error while reading file");
			e.printStackTrace();
		}
		sc.close();
	}

	/**
	 * Writes output to command line.
	 * 
	 * @param filename
	 *            name of input file
	 */
	private static void writeOutput(String filename) {
//		File file = new File(filename + ".out");
//		FileWriter fw = null;
//		BufferedWriter bw = null;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < allFactorials.length; ++i) {
			sb.append(allFactorials[i] + System.getProperty("line.separator"));
			System.out.println(allFactorials[i]);
		}
//		try {
//			fw = new FileWriter(file);
//			bw = new BufferedWriter(fw);
//			bw.write(sb.toString());
//			bw.close();
//			fw.close();
//		} catch (IOException e) {
//			System.out.println("Error while writing file.");
//			e.printStackTrace();
//		}
	}

}
