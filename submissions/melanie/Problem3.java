package melanie;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * 
 * @author Melanie Thissen
 * 
 */
public class Problem3 {

	private static int[][] testCases;
	private static double[] solutions;
	private static int[] prims = new int[] { 2, 3, 5, 7, 11, 13, 17 };

	public static void main(String[] args) {
		readFile("problem input/Football is simple/input.txt");
		solve();
		writeOutput("");
	}

	private static void solve() {
		for (int i = 0; i < testCases.length; ++i) {
			double prob1 = 0.0;
			double prob2 = 0.0;
			for (int j = 0; j < prims.length; ++j) {
				if (prims[j] > testCases[i][0]) {
					break;
				} else {
					prob1 += computeProbability(testCases[i][0],
							testCases[i][1], prims[j]);
					prob2 += computeProbability(testCases[i][0],
							testCases[i][2], prims[j]);
				}
			}
			solutions[i] = prob1 * prob2 + (1 - prob1) * prob2 + prob1
					* (1 - prob2);
		}
	}

	private static double computeProbability(int intervals, int probSucc,
			int prim) {
		long nfac = 1;
		long kfac = 1;
		long nMinusK = 1;
		for (int i = 2; i <= intervals; ++i) {
			nfac *= i;
		}
		for (int i = 2; i <= prim; ++i) {
			kfac *= i;
		}
		for (int i = 2; i <= intervals - prim; ++i) {
			nMinusK *= i;
		}
		double coeff = nfac / (kfac * nMinusK);
		double prob1 = probSucc / 100.0;
		prob1 = Math.pow(prob1, prim);
		double prob2 = (intervals - prim == 0) ? 1.0 : (1 - probSucc / 100.0);
		prob2 = Math.pow(prob2, intervals - prim);
		double result = (coeff * prob1 * prob2);
		return result;
	}

	@SuppressWarnings("resource")
	private static void readFile(String filename) {
		File file = new File(filename);
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			// read first line
			int numberOfCases = 0;
			if (sc.hasNextLine()) {
				String line = sc.nextLine().trim();
				if (line.matches("[0-9]+")) {
					numberOfCases = Integer.parseInt(line);
				} else {
					throw new RuntimeException("Error in first line");
				}
			} else {
				throw new RuntimeException("File is empty");
			}
			testCases = new int[numberOfCases][3];
			solutions = new double[numberOfCases];
			// read numbers
			for (int i = 0; i < numberOfCases; ++i) {
				if (sc.hasNextLine()) {
					String line = sc.nextLine().trim();
					String split[] = line.split(" ");
					testCases[i][0] = Integer.parseInt(split[0]);
					testCases[i][1] = Integer.parseInt(split[1]);
					testCases[i][2] = Integer.parseInt(split[2]);
				} else {
					throw new RuntimeException("File contains less lines than "
							+ numberOfCases);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error while reading file");
			e.printStackTrace();
		}
		sc.close();
	}

	/**
	 * Writes output to command line.
	 * 
	 * @param filename
	 *            name of input file
	 */
	private static void writeOutput(String filename) {
		// File file = new File(filename + ".out");
		// FileWriter fw = null;
		// BufferedWriter bw = null;
		StringBuilder sb = new StringBuilder();
		DecimalFormat f = new DecimalFormat("#0.0000");
		for (int i = 0; i < solutions.length; ++i) {
			sb.append(f.format(solutions[i])
					+ System.getProperty("line.separator"));
		}
		System.out.println(sb.toString());
		// try {
		// fw = new FileWriter(file);
		// bw = new BufferedWriter(fw);
		// bw.write(sb.toString());
		// bw.close();
		// fw.close();
		// } catch (IOException e) {
		// System.out.println("Error while writing file.");
		// e.printStackTrace();
		// }
	}
}
