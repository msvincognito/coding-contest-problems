package melanie;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import melanie.Field3D.Axis;

/**
 * 
 * @author Melanie Thissen
 * 
 */
public class Problem4 {

	private static int[] allCubes;
	private static int[] allEdgeCounts;
	private static HashMap<Integer, List<Field3D>> allVisitedpositions = new HashMap<Integer, List<Field3D>>();

	public static void main(String[] args) {
		// if (args != null && args.length <= 2 && args[1] != null
		// && args[1].length() > 0) {
		// readFile(args[1]);
		readFile("problem input/Match Model/sample input.txt");
		findEdgeCounts();
		writeOutput("submissions/melanie/problem4.txt");
		// writeOutput(args[1]);
		// } else {
		// System.out.println("No input file provided.");
		// }
	}

	private static void findEdgeCounts() {
		for (int i = 0; i < allCubes.length; ++i) {
			allVisitedpositions = new HashMap<Integer, List<Field3D>>();
			allEdgeCounts[i] = computeEdgesForCubes(allCubes[i]);
		}
	}

	private static int computeEdgesForCubes(int cubes) {
		if (cubes >= 1) {
			for (int i = 1; i <= cubes; ++i) {
				allVisitedpositions.put(i, new ArrayList<Field3D>());
			}
			Field3D field = new Field3D(cubes * 2 - 1);
			field.addCube(0, 0, 0);
			allVisitedpositions.get(1).add(field);
			return visitNode(field, cubes);
		}
		return 12 * cubes;
	}

	private static int visitNode(Field3D field, int maxCubeCount) {
		if (field.getNumberOfCubes() == maxCubeCount) {
			return field.countEdges3();
		}
		List<Field3D> children = createChildren(field);
		int min = maxCubeCount * 12;
		for (Field3D child : children) {
			int edgeCount = visitNode(child, maxCubeCount);
			if (edgeCount < min) {
				min = edgeCount;
			}
		}
		return min;
	}

	private static List<Field3D> createChildren(Field3D field) {
		List<Field3D> children = new ArrayList<Field3D>();
		List<Field3D> visited = allVisitedpositions.get(field
				.getNumberOfCubes() + 1);
		int shift = field.getShift();
		for (int i = -shift; i <= shift; ++i) {
			for (int j = -shift; j <= shift; ++j) {
				for (int k = -shift; k <= shift; ++k) {
					if (field.isPlaceForNewCube(i, j, k)) {
						field.addCube(i, j, k);
						if (!visited.contains(field)) {
							children.add(field.clone());
							visited.add(field.clone());
							List<Field3D> changedFields = getRotationsAndMirrors(field);
							for (Field3D changed : changedFields) {
								if (!visited.contains(changed)) {
									visited.add(changed.clone());
								}
							}
						}
						field.removeCube(i, j, k);
					}
				}
			}
		}
		return children;
	}

	/**
	 * An incomplete and not very sophisticated way of creating mirrored and
	 * rotated versions of the current field.
	 * 
	 * @param field
	 * @return
	 */
	private static List<Field3D> getRotationsAndMirrors(Field3D field) {
		List<Field3D> changedFields = new ArrayList<Field3D>();
		// mirrors
		Field3D changed = field.mirror(Axis.X);
		changedFields.add(changed.clone());
		changed = field.mirror(Axis.Y);
		changedFields.add(changed.clone());
		changed = field.mirror(Axis.Z);
		changedFields.add(changed.clone());
		changed = field.mirror(Axis.X).mirror(Axis.Y);
		changedFields.add(changed.clone());
		changed = field.mirror(Axis.X).mirror(Axis.Z);
		changedFields.add(changed.clone());
		changed = field.mirror(Axis.Y).mirror(Axis.Z);
		changedFields.add(changed.clone());

		// simple rotations
		changed = field.turn(Axis.X);
		changedFields.add(changed.clone());
		changed = field.turn(Axis.X).turn(Axis.X);
		changedFields.add(changed.clone());
		changed = field.turn(Axis.X).turn(Axis.X).turn(Axis.X);
		changedFields.add(changed.clone());
		changed = field.turn(Axis.Y);
		changedFields.add(changed.clone());
		changed = field.turn(Axis.Y).turn(Axis.Y);
		changedFields.add(changed.clone());
		changed = field.turn(Axis.Y).turn(Axis.Y).turn(Axis.Y);
		changedFields.add(changed.clone());
		changed = field.turn(Axis.Z);
		changedFields.add(changed.clone());
		changed = field.turn(Axis.Z).turn(Axis.Z);
		changedFields.add(changed.clone());
		changed = field.turn(Axis.Z).turn(Axis.Z).turn(Axis.Z);
		changedFields.add(changed.clone());
		return changedFields;
	}

	private static void readFile(String filename) {
		File file = new File(filename);
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			// read first line
			int numberOfCases = 0;
			if (sc.hasNextLine()) {
				String line = sc.nextLine().trim();
				if (line.matches("[0-9]+")) {
					numberOfCases = Integer.parseInt(line);
					allCubes = new int[numberOfCases];
					allEdgeCounts = new int[numberOfCases];
				} else {
					throw new RuntimeException("Error in first line");
				}
			} else {
				throw new RuntimeException("File is empty");
			}
			// read numbers
			for (int i = 0; i < numberOfCases; ++i) {
				if (sc.hasNextLine()) {
					allCubes[i] = Integer.parseInt(sc.nextLine().trim());
				} else {
					throw new RuntimeException("File contains less lines than "
							+ numberOfCases);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error while reading file");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error while parsing file");
			e.printStackTrace();
		}
		sc.close();
	}

	/**
	 * Writes output to command line.
	 * 
	 * @param filename
	 *            name of input file
	 */
	private static void writeOutput(String filename) {
		// File file = new File(filename + ".out");
		// FileWriter fw = null;
		// BufferedWriter bw = null;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < allEdgeCounts.length; ++i) {
			sb.append(allEdgeCounts[i] + System.getProperty("line.separator"));
			System.out.println(allEdgeCounts[i]);
		}
		// try {
		// fw = new FileWriter(file);
		// bw = new BufferedWriter(fw);
		// bw.write(sb.toString());
		// bw.close();
		// } catch (IOException e) {
		// System.out.println("Error while writing file.");
		// e.printStackTrace();
		// }
	}

}
