package melanie;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * largest found factorisation so far: 987532416: [[1, 2, 3, 4, 6, 8, 13, 23,
 * 47, 61]]
 * 
 * @author Melanie Thissen
 * 
 */
public class Problem5 {

	private static List<Integer> udns = new ArrayList<Integer>();
	private static HashMap<Integer, List<List<Integer>>> allFactorisations = new HashMap<Integer, List<List<Integer>>>();
	private static int maxLength = 0;

	public static void main(String[] args) {
		try {
			readFile("src/task5/udns.txt");
			System.out.println("Finished reading");
			List<List<Integer>> dummy = new ArrayList<List<Integer>>();
			List<Integer> dummy2 = new ArrayList<Integer>();
			dummy2.add(1);
			dummy.add(dummy2);
			allFactorisations.put(1, dummy);
			udns.remove(0);
			List<Integer> reverse = new ArrayList<Integer>();
			reverse.addAll(udns);
			Collections.reverse(reverse);
			for (int udn : reverse) {
				if (udn >= 985760421 || udn <= 62378) {
					continue;
				}

				dummy = new ArrayList<List<Integer>>();
				dummy2 = new ArrayList<Integer>();
				dummy2.add(1);
				dummy2.add(udn);
				dummy.add(dummy2);
				allFactorisations.put(udn, dummy);

				List<Integer> smallerUdns = new ArrayList<Integer>();
				List<Integer> factors = new ArrayList<Integer>();
				for (int i = 0; i < udns.size(); ++i) {
					if (udns.get(i) <= udn) {
						smallerUdns.add(udns.get(i));
					} else {
						break;
					}
				}
				findFactorisation(udn, udn, smallerUdns, factors);
				writeSolution(udn, allFactorisations.get(udn));
			}
			int max = 1;
			for (int key : allFactorisations.keySet()) {
				if (allFactorisations.get(key).get(0).size() > maxLength) {
					maxLength = allFactorisations.get(key).get(0).size();
					max = key;
				}
			}
			System.out.println(max + ": " + allFactorisations.get(max));
			writeSolution(max, allFactorisations.get(max));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	private static void findFactorisation(int udn, int orig,
			List<Integer> smallerUdns, List<Integer> factors) {
		if (udn > 1) {
			for (int i = 0; i < smallerUdns.size(); ++i) {
				int divider = smallerUdns.get(i);
				if (udn % divider == 0) {
					int result = udn / divider;
					if (result == divider || factors.contains(divider)) {
						continue;
					}
					List<Integer> tempSmaller = new ArrayList<Integer>();
					List<Integer> tempFactors = new ArrayList<Integer>();
					tempFactors.addAll(factors);
					for (int j = 0; j < smallerUdns.size(); ++j) {
						if (smallerUdns.get(j) <= result) {
							tempSmaller.add(smallerUdns.get(j));
						} else {
							break;
						}
					}
					tempFactors.add(divider);
					findFactorisation(result, orig, tempSmaller, tempFactors);
				}
			}
		} else {
			List<List<Integer>> temp = allFactorisations.get(orig);
			Collections.sort(factors);
			if (!factors.contains(1)) {
				factors.add(0, 1);
			}
			if (!temp.contains(factors)) {
				if (factors.size() > temp.get(0).size()) {
					temp = new ArrayList<List<Integer>>();
					temp.add(factors);
					allFactorisations.put(orig, temp);
				} else if (factors.size() == temp.get(0).size()) {
					temp.add(factors);
				}
			}
		}
	}

	private static void readFile(String filename) {
		File file = new File(filename);
		Scanner sc = null;
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				udns.add(Integer.parseInt(sc.nextLine().trim()));
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error while reading file");
			e.printStackTrace();
		}
		sc.close();
	}

	/**
	 * Searches for all UDNs and outputs them into a file. (Takes some minutes
	 * to run)
	 */
	public static void findUdns() {
		List<String> udns = new ArrayList<String>();
		for (int i = 1; i < 1000000000; ++i) {
			String udn = "" + i;
			boolean isUdn = true;
			for (int j = 0; j <= 9; ++j) {
				if (udn.indexOf("" + j) != udn.lastIndexOf("" + j)) {
					isUdn = false;
					break;
				}
			}
			if (isUdn) {
				udns.add(udn);
			}
		}
		writeOutput(udns);
	}

	/**
	 * Writes a list of strings into a file.
	 * 
	 * @param filename
	 *            name of input file
	 */
	private static void writeOutput(List<String> udn) {
		File file = new File("src/task5/udns.txt");
		FileWriter fw = null;
		BufferedWriter bw = null;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < udn.size(); ++i) {
			sb.append(udn.get(i) + System.getProperty("line.separator"));
		}
		try {
			fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			bw.write(sb.toString());
			bw.close();
		} catch (IOException e) {
			System.out.println("Error while writing file.");
			e.printStackTrace();
		}
	}

	private static void writeSolution(int max, List<List<Integer>> factors) {
		File file = new File("src/task5/solution.txt");
		FileWriter fw = null;
		BufferedWriter bw = null;
		StringBuilder sb = new StringBuilder();
		sb.append(max + ": " + factors + System.getProperty("line.separator"));
		try {
			fw = new FileWriter(file, true);
			bw = new BufferedWriter(fw);
			bw.write(sb.toString());
			bw.close();
		} catch (IOException e) {
			System.out.println("Error while writing file.");
			e.printStackTrace();
		}
	}

}
