package melanie;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Judging: produces 3448 characters 
 *
 * @author Melanie Thissen
 * 
 */
public class Problem8 {

	private static List<String> inputStrings = new ArrayList<String>();

	public static void main(String[] args) {
		readFile("../problem input/Who cares about readability/input.txt");
		for (String input : inputStrings)
			useLoop(input);
		System.out.println();
	}

	/**
	 * Translates a string into Brainfuck code in the most simple way.
	 */
	private static void notEvenTrying() {
		String input = "Hello World!";
		StringBuilder sb = new StringBuilder();
		for (int index = 0; index < input.length(); ++index) {
			for (int i = 0; i < (int) input.charAt(index); ++i) {
				sb.append("+");
			}
			sb.append(".>");
		}
		sb.setLength(sb.length() - 1);
		System.out.println(sb.toString());
		// System.out.println(sb.length());
	}

	/**
	 * Translates a string into Brainfuck code but shifts back to the first
	 * occurence of a letter if it appears again.
	 * 
	 * @param input
	 */
	private static void useDoubledChars(String input) {
		StringBuilder sb = new StringBuilder();
		for (int index = 0; index < input.length(); ++index) {
			int difference = index - input.indexOf(input.charAt(index));
			if (difference != 0) {
				// char already used
				StringBuilder sbTemp = new StringBuilder();
				for (int i = 0; i < difference; ++i) {
					sb.append("<");
					sbTemp.append(">");
				}
				sb.append("." + sbTemp.toString() + ">");
			} else {
				for (int i = 0; i < (int) input.charAt(index); ++i) {
					sb.append("+");
				}
				sb.append(".>");
			}
		}
		sb.setLength(sb.length() - 1);
		System.out.println(sb.toString());
		// System.out.println(sb.length());
	}

	/**
	 * Translates a string into Brainfuck code using a loop to initially
	 * increment all fields. Also shifts back to the first occurence of a letter
	 * if it appears again.
	 * 
	 * @param input
	 */
	private static void useLoop(String input) {
		int[] inputAsNumbers = new int[input.length()];
		int max = 0;
		int min = 129;
		for (int i = 0; i < input.length(); ++i) {
			int current = (int) input.charAt(i);
			inputAsNumbers[i] = current;
			if (current > max) {
				max = current;
			}
			if (current < min) {
				min = current;
			}
		}
		// System.out.println(Arrays.toString(inputAsNumbers));
		// only one char or one char repeated
		if (min == max) {
			useDoubledChars(input);
		} else {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < min; ++i) {
				sb.append("+");
				for (int j = 0; j < inputAsNumbers.length; ++j) {
					--inputAsNumbers[j];
				}
			}
			// loop for incrementing
			sb.append("[>");
			for (int index = 0; index < input.length(); ++index) {
				int difference = index - input.indexOf(input.charAt(index));
				if (difference != 0) {
					// char already used
					if (index < input.length() - 1) {
						sb.append(">");
					}
					inputAsNumbers[index] = -difference;
				} else {
					int border = ((int) input.charAt(index)) / min;
					if (inputAsNumbers[index] > 0) {
						inputAsNumbers[index] -= (border - 1) * min;
					}
					for (int i = 0; i < border; ++i) {
						sb.append("+");
					}
					if (index < input.length() - 1) {
						sb.append(">");
					}
				}
			}
			for (int i = 0; i < input.length(); ++i) {
				sb.append("<");
			}
			// end loop
			sb.append("-]>");
			// last incrementing and output
			for (int index = 0; index < input.length(); ++index) {
				if (inputAsNumbers[index] == 0) {
					if (index < input.length() - 1) {
						sb.append(".>");
					} else {
						sb.append(".");
					}
				} else if (inputAsNumbers[index] < 0) {
					StringBuilder sbTemp = new StringBuilder();
					for (int i = 0; i < -inputAsNumbers[index]; ++i) {
						sb.append("<");
						sbTemp.append(">");
					}
					if (index < input.length() - 1) {
						sb.append("." + sbTemp.toString() + ">");
					} else {
						sb.append(".");
					}
				} else {
					for (int i = 0; i < inputAsNumbers[index]; ++i) {
						sb.append("+");
					}
					if (index < input.length() - 1) {
						sb.append(".>");
					} else {
						sb.append(".");
					}
				}
			}
			System.out.println(sb.toString());
			// System.out.println(sb.length());
			// System.out.println(Arrays.toString(inputAsNumbers));
		}
	}

	private static void readFile(String filename) {
		File file = new File(filename);
		Scanner sc = null;
		int max = 0;
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) {
				inputStrings.add(sc.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error while reading file");
			e.printStackTrace();
		}
		sc.close();
	}
}
