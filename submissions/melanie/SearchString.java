package melanie;

import java.util.Observable;
import java.util.Observer;

/**
 * 
 * @author Melanie Thissen
 * 
 */
public class SearchString implements Observer {

	/**
	 * Current index.
	 */
	private int index = 1;
	/**
	 * The search string that is to be matched.
	 */
	private String searchString = "";
	/**
	 * Will be set to false if a character does not match.
	 */
	private boolean canFit = true;
	/**
	 * Will be set to true if the end of the search string is reached and all
	 * characters were matched.
	 */
	private boolean finished = false;

	public SearchString(String s) {
		this.searchString = s;
		if (s.length() == 1) {
			finished = true;
		}
	}

	/**
	 * Checks if the next character from the text matches the search string and
	 * sets a flag otherwise. If the end of the search string is reached,
	 * another flag is set accordingly.
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		char nextChar = Character.valueOf((Character) arg1);
		if (nextChar == searchString.charAt(index)) {
			if (index == searchString.length() - 1) {
				finished = true;
			}
		} else {
			canFit = false;
		}
		++index;
	}

	public String getSearchString() {
		return searchString;
	}

	public boolean isCanFit() {
		return canFit;
	}

	public boolean isFinished() {
		return finished;
	}

	public String toString() {
		return searchString;
	}

}
